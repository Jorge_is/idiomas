$(document).ready(function () {

    //Data Table js
    $('#myTable').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
    });

    //Boostrap DatePicker js
    $('.js-datepicker').datepicker({
        language: 'es',
        format: 'yyyy-mm-dd'
    });

    $("#pais").change(function () {
        var data = {
            pais_id: $(this).val()
        };

        $.ajax({
            type: 'post',
            url: URL + '/admin/cat-cursos/dynamic-ciudades',
            data: data,
            success: function (data) {
                console.log(data);
                var $province_selector = $('#ciudad');

                $province_selector.html('<option disabled selected="selected">Ciudades</option>');

                for (var i = 0, total = data.length; i < total; i++) {
                    $province_selector.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                }

                $('#escuela').html("<option>Sedes</option>");
            }
        });
    });

    $("#ciudad").change(function () {
        var data = {
            ciudad_id: $(this).val()
        };

        $.ajax({
            type: 'post',
            url: URL + '/admin/cat-cursos/dynamic-escuelas',
            data: data,
            success: function (data) {
                var $city_selector = $('#escuela');

                $city_selector.html('<option disabled selected="selected">Sedes</option>');

                for (var i = 0, total = data.length; i < total; i++) {
                    $city_selector.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                }

                $('#curso').html("<option>Cursos</option>");
            }
        });
    });

    $("#escuela").change(function () {
        var data = {
            escuela_id: $(this).val()
        };

        $.ajax({
            type: 'post',
            url: URL + '/admin/cat-cursos/dynamic-cursos',
            data: data,
            success: function (data) {
                var $city_selector = $('#curso');

                $city_selector.html('<option disabled selected="selected">Cursos</option>');

                for (var i = 0, total = data.length; i < total; i++) {
                    $city_selector.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                }
            }
        });
    });

    //Search curso
    $('#search').keyup(function () {
        var txt = $(this).val();

        if (txt != "") {
            $.ajax({
                type: 'post',
                url: URL + '/admin/curso-content/search-course',
                data: {search: txt},
                success: function (data) {

                    $('ul#result').empty();
                    $.each(data, function (index) {
                        $('ul#result').append('<li><a href="#" data-name="' + this.name + '">' + this.name + '</a></li>');
                        var dom = $('ul#result li a');

                        dom.on('click', function () {
                            var course = $(this).data('name');

                            $('#curso-dinamico').val(course);

                        });
                    });

                }
            });
        } else {
            $('ul#result').empty();
            $('#curso-dinamico').val("");
        }
    });

    //Search escuela
    $('#search-escuela').keyup(function () {
        var txt = $(this).val();

        if (txt != "") {
            $.ajax({
                type: 'post',
                url: URL + '/admin/catescuelas/search-escuela',
                data: {search: txt},
                success: function (data) {

                    $('ul#result').empty();
                    $.each(data, function (index) {
                        $('ul#result').append('<li><a href="#" data-name="' + this.name + '" data-sede="' + this.ciudad + '" + data-pais = "' + this.pais + '">' + this.name + " - " + this.ciudad + " - " + this.pais + '</a></li>');
                        var dom = $('ul#result li a');

                        dom.on('click', function () {
                            var course = $(this).data('name');
                            var sede = $(this).data('sede');
                            var pais = $(this).data('pais');

                            $('#escuela-dinamico').val(course);
                            $('#sede-dinamico').val(sede);
                            $('#pais-dinamico').val(pais);
                            $('ul#result').empty();

                        });
                    });

                }
            });
        } else {
            $('ul#result').empty();
            $('#escuela-dinamico').val("");
        }
    });

    //DELETE IMG DEVICE
    $('#gallery-wrapper .img-close').each(function (index) {
        $(this).on('click', function () {

            var id_image = $(this).data("id");
            var id_photogallery = $(this).data("photogallery-id");
            var entity = $(".remove-img-photogallery").data("entity");

            var data = {
                image_id: id_image,
                photogallery_id: id_photogallery
            }

            $.ajax({
                    type: "post",
                    url: URL + `/admin/${entity}/delete-image`,
                    data: data,
                    success: function (data) {
                        console.log(data);
                    },
                    error: function () {
                    }
                }
            );
        });
    });

    //DELETE VIDEO DEVICE
    $('#gallery-wrapper .img-close-video').each(function (index) {
        $(this).on('click', function () {

            var id_video = $(this).data("id");
            var id_photogallery = $(this).data("photogallery-id");
            var entity = $(".remove-img-photogallery").data("entity");

            var data = {
                video_id: id_video,
                photogallery_id: id_photogallery
            }

            $.ajax({
                    type: "post",
                    url: URL + `/admin/${entity}/delete-video`,
                    data: data,
                    success: function (data) {
                        console.log(data);
                    },
                    error: function () {
                    }
                }
            );
        });
    });

    $("#blocks-extras").show();
    //combo secciones
    $(".combo-secciones").change(function () {
        if ($(".combo-secciones option:selected").text() == 'Curso') {
            $("#blocks-extras").hide();
        } else if ($(".combo-secciones option:selected").text() == 'Fotos_Y_Videos') {
            $("#blocks-extras").hide();
        } else {
            $("#blocks-extras").show();
        }
    });

    //******Uploads dinamyc*****
    var $collectionHolder1;
    var $collectionHolder2;
    var $collectionHolder3;

    // setup an "add a tag" link
    var $addTagLink1 = $('<a href="#" class="add_tag_link_1">Add a file</a>');
    var $newLinkLi1 = $('<li></li>').append($addTagLink1);

    var $addTagLink2 = $('<a href="#" class="add_tag_link_2">Add a file</a>');
    var $newLinkLi2 = $('<li></li>').append($addTagLink2);

    var $addTagLink3 = $('<a href="#" class="add_tag_link_3">Add a file</a>');
    var $newLinkLi3 = $('<li></li>').append($addTagLink3);


    // Get the ul that holds the collection of tags
    $collectionHolder1 = $('ul.files-actividades');
    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder1.append($newLinkLi1);
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder1.data('index', $collectionHolder1.find(':input').length);
    $addTagLink1.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        // add a new tag form (see next code block)
        addTagForm1($collectionHolder1, $newLinkLi1);
    });

    // Get the ul that holds the collection of tags
    $collectionHolder2 = $('ul.files-galerias');
    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder2.append($newLinkLi2);
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder2.data('index', $collectionHolder2.find(':input').length);
    $addTagLink2.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        // add a new tag form (see next code block)
        addTagForm2($collectionHolder2, $newLinkLi2);
    });

    // Get the ul that holds the collection of tags
    $collectionHolder3 = $('ul.files-videos');
    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder3.append($newLinkLi3);
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder3.data('index', $collectionHolder3.find(':input').length);
    $addTagLink3.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        // add a new tag form (see next code block)
        addTagForm3($collectionHolder3, $newLinkLi3);
    });


});

function addTagForm1($collectionHolder1, $newLinkLi1) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder1.data('prototype');
    // get the new index
    var index = $collectionHolder1.data('index');
    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);
    // increase the index with one for the next item
    $collectionHolder1.data('index', index + 1);
    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi1.before($newFormLi);
}

function addTagForm2($collectionHolder2, $newLinkLi2) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder2.data('prototype');
    // get the new index
    var index = $collectionHolder2.data('index');
    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);
    // increase the index with one for the next item
    $collectionHolder2.data('index', index + 1);
    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi2.before($newFormLi);
}

function addTagForm3($collectionHolder3, $newLinkLi3) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder3.data('prototype');
    // get the new index
    var index = $collectionHolder3.data('index');
    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);
    // increase the index with one for the next item
    $collectionHolder3.data('index', index + 1);
    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi3.before($newFormLi);
}





