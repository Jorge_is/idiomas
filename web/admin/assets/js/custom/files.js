$(document).ready(function () {

    /*$("#grados").change(function () {
        var data = {
            grade_id: $(this).val()
        };

        $.ajax({
            type: 'post',
            url: URL + '/dynamic-materias',
            data: data,
            success: function (data) {
                console.log(data);
                var $province_selector = $('#materias');

                $province_selector.html('<option disabled selected="selected">Materias</option>');

                for (var i = 0, total = data.length; i < total; i++) {
                    $province_selector.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                }

                $('#bloques').html("<option>Bloques</option>");
            }
        });
    });*/


    $("#materias").change(function () {
        var data = {
            materia_id: $(this).val()
        };

        $.ajax({
            type: 'post',
            url: URL + '/dynamic-blocks',
            data: data,
            success: function (data) {
                var $city_selector = $('#bloques');

                $city_selector.html('<option disabled selected="selected">Bloques</option>');

                for (var i = 0, total = data.length; i < total; i++) {
                    $city_selector.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                }
            }
        });
    });
    
    $("#bloques").change(function () {
        var data = {
            block_id: $(this).val()
        };

        $.ajax({
            type: 'post',
            url: URL + '/dynamic-weeks',
            data: data,
            success: function (data) {
                var $city_selector = $('#semanas');

                $city_selector.html('<option disabled selected="selected">Semanas</option>');

                for (var i = 0, total = data.length; i < total; i++) {
                    $city_selector.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                }
            }
        });
    });

});


