<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class CatCiudadesEscuelasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ciudadDesc', TextType::class, array(
                'label' => 'Nombre',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('ciudadDescLarga', CKEditorType::class, array(
                'label' => 'Descripcion Larga',
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ))
            ->add('idPais', EntityType::class, array(
                'class' => 'AdminBundle:CatPaisesEscuelas',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.paisDesc', 'ASC');
                },
                'choice_label' => 'paisDesc',
                'label' => 'Pais',
                'attr' => array(
                    'class' => 'form-control m-xs-b-2')
            ))
            ->add('imgBandera', FileType::class, array(
                'label' => 'Imagen Bandera',
                'required' => 'required',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('imgModal', FileType::class, array(
                'label' => 'Imagen Modal',
                'required' => 'required',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ));
            /*->add('cd01')
            ->add('cd02')
            ->add('idPaisEdo')
            ->add('idEstadoEdo')
            ->add('cd05')
            ->add('cd06')
            ->add('cd07')
            ->add('cd08')
            ->add('cd09')
            ->add('cd10')
            ->add('cd11')
            ->add('cd12')
            ->add('cd13')
            ->add('cd14')
            ->add('cd15')
            ->add('cd16')
            ->add('cd17')*/
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\CatCiudadesEscuelas'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_catciudadesescuelas';
    }


}
