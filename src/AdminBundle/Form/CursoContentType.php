<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class CursoContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bloqueDescripcion', CKEditorType::class, array(
                'label' => 'Bloque Descripción',
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ))
            ->add('bloqueRequisitos', CKEditorType::class, array(
                'label' => 'Bloque Requisitos',
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ))
            ->add('bloqueCaracteristicas', CKEditorType::class, array(
                'label' => 'Bloque Caracteristicas',
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\CursoContent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_cursocontent';
    }


}
