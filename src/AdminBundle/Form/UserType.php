<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = array(
            "Admin" => "ROLE_ADMIN",
            "Editor" => "ROLE_EDITOR"
        );

        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nombre',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('aPterno', TextType::class, array(
                'label' => 'Apellido Paterno',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('aMaterno', TextType::class, array(
                'label' => 'Apellido Materno',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'Password',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('role', ChoiceType::class, array(
                'label' => 'Role',
                'placeholder' => 'Escoga una opción',
                'choices' => $roles,
                'required' => 'required',
                "attr" => array("class" => "status-form form-control m-xs-b-2")
            ))
            ->add('status', CheckboxType::class, array(
                'label' => 'Estado',
                'attr' => array('class' => 'checkbox-inline m-left')
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_user';
    }


}
