<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class CatPaisesEscuelasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paisDesc', TextType::class, array(
                'label' => 'Nombre',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('paisDescLarga', CKEditorType::class, array(
                'label' => 'Descripcion Larga',
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ))
            ->add('pa07', FileType::class, array(
                'label' => 'Imagen Bandera',
                'required' => 'required',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('imgModal', FileType::class, array(
                'label' => 'Imagen Modal',
                'required' => 'required',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ));
            /*->add('pa01')
            ->add('pa02')
            ->add('pa03')
            ->add('pa04')
            ->add('pa05')
            ->add('pa06')*/

            /*->add('pa08')
            ->add('pa09')
            ->add('pa10')
            ->add('pa11')
            ->add('pa12')
            ->add('pa13')*/
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\CatPaisesEscuelas'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_catpaisesescuelas';
    }


}
