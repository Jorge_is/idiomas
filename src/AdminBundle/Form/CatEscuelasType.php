<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class CatEscuelasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('escNombre', TextType::class, array(
                'label' => 'Nombre',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('escNombreCorto', TextType::class, array(
                'label' => 'Nombre Corto',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            /*->add('idPais', EntityType::class, array(
                'class' => 'AdminBundle:CatPaisesEscuelas',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.paisDesc', 'ASC');
                },
                'choice_label' => 'paisDesc',
                'label' => 'Pais',
                'attr' => array(
                    'class' => 'form-control m-xs-b-2')
            ))
            ->add('idCiudad', EntityType::class, array(
                'class' => 'AdminBundle:CatCiudadesEscuelas',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.ciudadDesc', 'ASC');
                },
                'choice_label' => 'ciudadDesc',
                'label' => 'Ciudad',
                'attr' => array(
                    'class' => 'form-control m-xs-b-2')
            ))*/
            ->add('esc19', FileType::class, array(
                'label' => 'Imagen',
                'required' => 'required',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('banner', FileType::class, array(
                'label' => 'Imagen Banner',
                'required' => 'required',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ));
            /*->add('idMon')
            ->add('esc01')
            ->add('esc02')
            ->add('esc03')
            ->add('esc04')
            ->add('esc05')
            ->add('esc06')
            ->add('esc07')
            ->add('esc08')
            ->add('esc09')
            ->add('esc10')
            ->add('esc11')
            ->add('esc12')
            ->add('esc13')
            ->add('esc14')
            ->add('esc15')
            ->add('esc16')
            ->add('esc17')
            ->add('esc18')
            ->add('esc20')
            ->add('esc21')
            ->add('esc22')
            ->add('esc23')
            ->add('esc24')
            ->add('esc25')
            ->add('esc26')
            ->add('esc27')
            ->add('esc28')
            ->add('esc29')
            ->add('esc30')*/;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\CatEscuelas'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_catescuelas';
    }


}
