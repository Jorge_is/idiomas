<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class BlockType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seccion', EntityType::class, array(
                'class' => 'AdminBundle:SeccionesEscuelas',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->where('m.active = :active')
                        ->andWhere('m.name = :seccion')
                        ->setParameter('seccion', 'Escuela')
                        ->setParameter('active', 1)
                        ->orderBy('m.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Seccion',
                'attr' => array(
                    'class' => 'form-control m-xs-b-2 combo-secciones')
            ))
            ->add('nameBlockOne', TextType::class, array(
                'label' => 'Nombre',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('contenBlockOne', CKEditorType::class, array(
                'label' => 'Bloque Descripción',
                'required' => false,
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ))
            ->add('nameBlockSecond', TextType::class, array(
                'label' => 'Nombre',
                'required' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('contenBlockSecond', CKEditorType::class, array(
                'label' => 'Bloque Descripción',
                'required' => false,
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ))
            ->add('nameBlockThird', TextType::class, array(
                'label' => 'Nombre',
                'required' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('contenBlockThird', CKEditorType::class, array(
                'label' => 'Bloque Descripción',
                'required' => false,
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Block'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_block';
    }


}
