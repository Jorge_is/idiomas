<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Doctrine\ORM\EntityRepository;
use AdminBundle\Form\ImageType;

class PhotogalleryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('fileActividad', FileType::class, array(
                'label' => 'Imagen Actividad',
                'required' => false,
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control')
            ))
            ->add('fileGaleria', FileType::class, array(
                'label' => 'Imagen Galeria',
                'required' => false,
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control')
            ))*/
            ->add('seccion', EntityType::class, array(
                'class' => 'AdminBundle:SeccionesEscuelas',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->where('m.active = :active')
                        ->andWhere('m.name = :seccion')
                        ->setParameter('seccion', 'Fotos_Y_Videos')
                        ->setParameter('active', 1)
                        ->orderBy('m.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Seccion',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-control m-xs-b-2 combo-secciones')
            ))
            ->add('fileActividad', CollectionType::class,array(
                'required' => false,
                'data_class' => null,
                'mapped' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr'      => array('class' => 'form-control')
                ),
            ))
            ->add('fileGaleria', CollectionType::class,array(
                'required' => false,
                'data_class' => null,
                'mapped' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr'      => array('class' => 'form-control')
                ),
            ))
            ->add('video', CollectionType::class,array(
                'required' => false,
                'data_class' => null,
                'mapped' => false,
                'entry_type' => TextareaType::class,
                'allow_add' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr'      => array('class' => 'form-control')
                ),
            ))
            /*->add('video', TextareaType::class, array(
                'data_class' => null,
                'mapped' => false,
                'required' => false,
                'attr' => array(
                    'class' => 'form-name form-control'
                )
            ))*/;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Photogallery'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_photogallery';
    }


}
