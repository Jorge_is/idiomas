<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class IdiomaContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Titulo',
                'required' => 'required',
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ))
            ->add('body', CKEditorType::class, array(
                'config' => array(
                    'language' => 'es',
                    'uiColor' => '#EEEEEE',
                    'toolbar' => 'basic'
                ),
                'attr' => array(
                    'class' => 'm-xs-b-2'
                )
            ))
            ->add('idioma', EntityType::class, array(
                'class' => 'AdminBundle:Idioma',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.name', 'ASC');
                },
                'choice_label' => 'name',
                'label' => 'Idioma',
                'attr' => array(
                    'class' => 'form-control m-xs-b-2')
            ))
            ->add('file', FileType::class, array(
                'label' => 'Imagen',
                'required' => 'required',
                'data_class' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-name form-control m-xs-b-2')
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\IdiomaContent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_idiomacontent';
    }


}
