<?php

namespace AdminBundle\Entity;

/**
 * CursoContent
 */
class CursoContent
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $bloqueDescripcion;

    /**
     * @var string
     */
    private $bloqueRequisitos;

    /**
     * @var string
     */
    private $bloqueCaracteristicas;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bloqueDescripcion
     *
     * @param string $bloqueDescripcion
     *
     * @return CursoContent
     */
    public function setBloqueDescripcion($bloqueDescripcion)
    {
        $this->bloqueDescripcion = $bloqueDescripcion;

        return $this;
    }

    /**
     * Get bloqueDescripcion
     *
     * @return string
     */
    public function getBloqueDescripcion()
    {
        return $this->bloqueDescripcion;
    }

    /**
     * Set bloqueRequisitos
     *
     * @param string $bloqueRequisitos
     *
     * @return CursoContent
     */
    public function setBloqueRequisitos($bloqueRequisitos)
    {
        $this->bloqueRequisitos = $bloqueRequisitos;

        return $this;
    }

    /**
     * Get bloqueRequisitos
     *
     * @return string
     */
    public function getBloqueRequisitos()
    {
        return $this->bloqueRequisitos;
    }

    /**
     * Set bloqueCaracteristicas
     *
     * @param string $bloqueCaracteristicas
     *
     * @return CursoContent
     */
    public function setBloqueCaracteristicas($bloqueCaracteristicas)
    {
        $this->bloqueCaracteristicas = $bloqueCaracteristicas;

        return $this;
    }

    /**
     * Get bloqueCaracteristicas
     *
     * @return string
     */
    public function getBloqueCaracteristicas()
    {
        return $this->bloqueCaracteristicas;
    }

    /**
     * @var \AdminBundle\Entity\CatCurso
     */
    private $curso;


    /**
     * Set curso
     *
     * @param \AdminBundle\Entity\CatCursos $curso
     *
     * @return CursoContent
     */
    public function setCurso(\AdminBundle\Entity\CatCursos $curso = null)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * Get curso
     *
     * @return \AdminBundle\Entity\CatCursos
     */
    public function getCurso()
    {
        return $this->curso;
    }
    /**
     * @var \AdminBundle\Entity\CatEscuelas
     */
    private $escuela;

    /**
     * @var \AdminBundle\Entity\Idioma
     */
    private $idioma;


    /**
     * Set escuela
     *
     * @param \AdminBundle\Entity\CatEscuelas $escuela
     *
     * @return CursoContent
     */
    public function setEscuela(\AdminBundle\Entity\CatEscuelas $escuela = null)
    {
        $this->escuela = $escuela;

        return $this;
    }

    /**
     * Get escuela
     *
     * @return \AdminBundle\Entity\CatEscuelas
     */
    public function getEscuela()
    {
        return $this->escuela;
    }

    /**
     * Set idioma
     *
     * @param \AdminBundle\Entity\Idioma $idioma
     *
     * @return CursoContent
     */
    public function setIdioma(\AdminBundle\Entity\Idioma $idioma = null)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return \AdminBundle\Entity\Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }
}
