<?php

namespace AdminBundle\Entity;

/**
 * CatPaisesEscuelas
 */
class CatPaisesEscuelas
{
    /**
     * @var integer
     */
    private $idPais;

    /**
     * @var string
     */
    private $paisDesc;

    /**
     * @var string
     */
    private $paisDescLarga;

    public function __toString() {
        return $this->getPaisDesc();
    }

    /**
     * Get idPais
     *
     * @return integer
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * Set paisDesc
     *
     * @param string $paisDesc
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDesc($paisDesc)
    {
        $this->paisDesc = $paisDesc;

        return $this;
    }

    /**
     * Get paisDesc
     *
     * @return string
     */
    public function getPaisDesc()
    {
        return $this->paisDesc;
    }

    /**
     * Set paisDescLarga
     *
     * @param string $paisDescLarga
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDescLarga($paisDescLarga)
    {
        $this->paisDescLarga = $paisDescLarga;

        return $this;
    }

    /**
     * Get paisDescLarga
     *
     * @return string
     */
    public function getPaisDescLarga()
    {
        return $this->paisDescLarga;
    }


    /**
     * @var \AdminBundle\Entity\Image
     */
    private $pa07;

    /**
     * @var \AdminBundle\Entity\Image
     */
    private $imgModal;


    /**
     * Set pa07
     *
     * @param \AdminBundle\Entity\Image $pa07
     *
     * @return CatPaisesEscuelas
     */
    public function setPa07(\AdminBundle\Entity\Image $pa07 = null)
    {
        $this->pa07 = $pa07;

        return $this;
    }

    /**
     * Get pa07
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getPa07()
    {
        return $this->pa07;
    }

    /**
     * Set imgModal
     *
     * @param \AdminBundle\Entity\Image $imgModal
     *
     * @return CatPaisesEscuelas
     */
    public function setImgModal(\AdminBundle\Entity\Image $imgModal = null)
    {
        $this->imgModal = $imgModal;

        return $this;
    }

    /**
     * Get imgModal
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getImgModal()
    {
        return $this->imgModal;
    }
    /**
     * @var string
     */
    private $pais_desc_gen;

    /**
     * @var string
     */
    private $pais_desc_idi;

    /**
     * @var string
     */
    private $pais_desc_camp;

    /**
     * @var string
     */
    private $pais_desc_hs;

    /**
     * @var string
     */
    private $pais_desc_esup;

    /**
     * @var string
     */
    private $pais_desc_ws;

    /**
     * @var string
     */
    private $pais_img_hs;

    /**
     * @var string
     */
    private $pais_ficha;

    /**
     * @var boolean
     */
    private $pais_ficha_npag_idi;

    /**
     * @var boolean
     */
    private $pais_ficha_npag_camp;

    /**
     * @var boolean
     */
    private $pais_ficha_npag_hs;

    /**
     * @var boolean
     */
    private $pais_ficha_npag_esup;

    /**
     * @var boolean
     */
    private $pais_ficha_npag_ws;


    /**
     * Set paisDescGen
     *
     * @param string $paisDescGen
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDescGen($paisDescGen)
    {
        $this->pais_desc_gen = $paisDescGen;

        return $this;
    }

    /**
     * Get paisDescGen
     *
     * @return string
     */
    public function getPaisDescGen()
    {
        return $this->pais_desc_gen;
    }

    /**
     * Set paisDescIdi
     *
     * @param string $paisDescIdi
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDescIdi($paisDescIdi)
    {
        $this->pais_desc_idi = $paisDescIdi;

        return $this;
    }

    /**
     * Get paisDescIdi
     *
     * @return string
     */
    public function getPaisDescIdi()
    {
        return $this->pais_desc_idi;
    }

    /**
     * Set paisDescCamp
     *
     * @param string $paisDescCamp
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDescCamp($paisDescCamp)
    {
        $this->pais_desc_camp = $paisDescCamp;

        return $this;
    }

    /**
     * Get paisDescCamp
     *
     * @return string
     */
    public function getPaisDescCamp()
    {
        return $this->pais_desc_camp;
    }

    /**
     * Set paisDescHs
     *
     * @param string $paisDescHs
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDescHs($paisDescHs)
    {
        $this->pais_desc_hs = $paisDescHs;

        return $this;
    }

    /**
     * Get paisDescHs
     *
     * @return string
     */
    public function getPaisDescHs()
    {
        return $this->pais_desc_hs;
    }

    /**
     * Set paisDescEsup
     *
     * @param string $paisDescEsup
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDescEsup($paisDescEsup)
    {
        $this->pais_desc_esup = $paisDescEsup;

        return $this;
    }

    /**
     * Get paisDescEsup
     *
     * @return string
     */
    public function getPaisDescEsup()
    {
        return $this->pais_desc_esup;
    }

    /**
     * Set paisDescWs
     *
     * @param string $paisDescWs
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisDescWs($paisDescWs)
    {
        $this->pais_desc_ws = $paisDescWs;

        return $this;
    }

    /**
     * Get paisDescWs
     *
     * @return string
     */
    public function getPaisDescWs()
    {
        return $this->pais_desc_ws;
    }

    /**
     * Set paisImgHs
     *
     * @param string $paisImgHs
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisImgHs($paisImgHs)
    {
        $this->pais_img_hs = $paisImgHs;

        return $this;
    }

    /**
     * Get paisImgHs
     *
     * @return string
     */
    public function getPaisImgHs()
    {
        return $this->pais_img_hs;
    }

    /**
     * Set paisFicha
     *
     * @param string $paisFicha
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisFicha($paisFicha)
    {
        $this->pais_ficha = $paisFicha;

        return $this;
    }

    /**
     * Get paisFicha
     *
     * @return string
     */
    public function getPaisFicha()
    {
        return $this->pais_ficha;
    }

    /**
     * Set paisFichaNpagIdi
     *
     * @param boolean $paisFichaNpagIdi
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisFichaNpagIdi($paisFichaNpagIdi)
    {
        $this->pais_ficha_npag_idi = $paisFichaNpagIdi;

        return $this;
    }

    /**
     * Get paisFichaNpagIdi
     *
     * @return boolean
     */
    public function getPaisFichaNpagIdi()
    {
        return $this->pais_ficha_npag_idi;
    }

    /**
     * Set paisFichaNpagCamp
     *
     * @param boolean $paisFichaNpagCamp
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisFichaNpagCamp($paisFichaNpagCamp)
    {
        $this->pais_ficha_npag_camp = $paisFichaNpagCamp;

        return $this;
    }

    /**
     * Get paisFichaNpagCamp
     *
     * @return boolean
     */
    public function getPaisFichaNpagCamp()
    {
        return $this->pais_ficha_npag_camp;
    }

    /**
     * Set paisFichaNpagHs
     *
     * @param boolean $paisFichaNpagHs
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisFichaNpagHs($paisFichaNpagHs)
    {
        $this->pais_ficha_npag_hs = $paisFichaNpagHs;

        return $this;
    }

    /**
     * Get paisFichaNpagHs
     *
     * @return boolean
     */
    public function getPaisFichaNpagHs()
    {
        return $this->pais_ficha_npag_hs;
    }

    /**
     * Set paisFichaNpagEsup
     *
     * @param boolean $paisFichaNpagEsup
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisFichaNpagEsup($paisFichaNpagEsup)
    {
        $this->pais_ficha_npag_esup = $paisFichaNpagEsup;

        return $this;
    }

    /**
     * Get paisFichaNpagEsup
     *
     * @return boolean
     */
    public function getPaisFichaNpagEsup()
    {
        return $this->pais_ficha_npag_esup;
    }

    /**
     * Set paisFichaNpagWs
     *
     * @param boolean $paisFichaNpagWs
     *
     * @return CatPaisesEscuelas
     */
    public function setPaisFichaNpagWs($paisFichaNpagWs)
    {
        $this->pais_ficha_npag_ws = $paisFichaNpagWs;

        return $this;
    }

    /**
     * Get paisFichaNpagWs
     *
     * @return boolean
     */
    public function getPaisFichaNpagWs()
    {
        return $this->pais_ficha_npag_ws;
    }

    /**
     * Set idPais
     *
     * @param integer $idPais
     *
     * @return CatPaisesEscuelas
     */
    public function setIdPais($idPais)
    {
        $this->idPais = $idPais;

        return $this;
    }
}
