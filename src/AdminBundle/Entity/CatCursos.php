<?php

namespace AdminBundle\Entity;

/**
 * CatCursos
 */
class CatCursos
{
    /**
     * @var integer
     */
    private $idCurso;

    /**
     * @var string
     */
    private $curNombre;

    /**
     * @var string
     */
    private $curDesc;

    /**
     * @var boolean
     */
    private $idPrograma;


    /**
     * Get idCurso
     *
     * @return integer
     */
    public function getIdCurso()
    {
        return $this->idCurso;
    }

    /**
     * Set idCurso
     *
     * @param integer $idCurso
     *
     * @return CatCursos
     */
    public function setIdCurso($idCurso)
    {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Set curNombre
     *
     * @param string $curNombre
     *
     * @return CatCursos
     */
    public function setCurNombre($curNombre)
    {
        $this->curNombre = $curNombre;

        return $this;
    }

    /**
     * Get curNombre
     *
     * @return string
     */
    public function getCurNombre()
    {
        return $this->curNombre;
    }

    /**
     * Set curDesc
     *
     * @param string $curDesc
     *
     * @return CatCursos
     */
    public function setCurDesc($curDesc)
    {
        $this->curDesc = $curDesc;

        return $this;
    }

    /**
     * Get curDesc
     *
     * @return string
     */
    public function getCurDesc()
    {
        return $this->curDesc;
    }

    /**
     * Set idPrograma
     *
     * @param boolean $idPrograma
     *
     * @return CatCursos
     */
    public function setIdPrograma($idPrograma)
    {
        $this->idPrograma = $idPrograma;

        return $this;
    }

    /**
     * Get idPrograma
     *
     * @return boolean
     */
    public function getIdPrograma()
    {
        return $this->idPrograma;
    }
}
