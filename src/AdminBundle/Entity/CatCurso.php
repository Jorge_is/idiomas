<?php

namespace AdminBundle\Entity;

/**
 * CatCurso
 */
class CatCurso
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    public function __toString() {
        return $this->getName();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CatCurso
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \AdminBundle\Entity\CatEscuelas
     */
    private $escuela;


    /**
     * Set escuela
     *
     * @param \AdminBundle\Entity\CatEscuelas $escuela
     *
     * @return CatCurso
     */
    public function setEscuela(\AdminBundle\Entity\CatEscuelas $escuela = null)
    {
        $this->escuela = $escuela;

        return $this;
    }

    /**
     * Get escuela
     *
     * @return \AdminBundle\Entity\CatEscuelas
     */
    public function getEscuela()
    {
        return $this->escuela;
    }
    /**
     * @var \AdminBundle\Entity\Idioma
     */
    private $idioma;


    /**
     * Set idioma
     *
     * @param \AdminBundle\Entity\Idioma $idioma
     *
     * @return CatCurso
     */
    public function setIdioma(\AdminBundle\Entity\Idioma $idioma = null)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return \AdminBundle\Entity\Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }
    /**
     * @var string
     */
    private $cur_desc;

    /**
     * @var string
     */
    private $id_programa;


    /**
     * Set curDesc
     *
     * @param string $curDesc
     *
     * @return CatCurso
     */
    public function setCurDesc($curDesc)
    {
        $this->cur_desc = $curDesc;

        return $this;
    }

    /**
     * Get curDesc
     *
     * @return string
     */
    public function getCurDesc()
    {
        return $this->cur_desc;
    }

    /**
     * Set idPrograma
     *
     * @param string $idPrograma
     *
     * @return CatCurso
     */
    public function setIdPrograma($idPrograma)
    {
        $this->id_programa = $idPrograma;

        return $this;
    }

    /**
     * Get idPrograma
     *
     * @return string
     */
    public function getIdPrograma()
    {
        return $this->id_programa;
    }
}
