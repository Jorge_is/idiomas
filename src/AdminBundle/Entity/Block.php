<?php

namespace AdminBundle\Entity;

/**
 * Block
 */
class Block
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \AdminBundle\Entity\Escuela
     */
    private $escuela;

    /**
     * @var \AdminBundle\Entity\Seccion
     */
    private $seccion;


    /**
     * Set escuela
     *
     * @param \AdminBundle\Entity\CatEscuelas $escuela
     *
     * @return Block
     */
    public function setEscuela(\AdminBundle\Entity\CatEscuelas $escuela = null)
    {
        $this->escuela = $escuela;

        return $this;
    }

    /**
     * Get escuela
     *
     * @return \AdminBundle\Entity\CatEscuelas
     */
    public function getEscuela()
    {
        return $this->escuela;
    }

    /**
     * Set seccion
     *
     * @param \AdminBundle\Entity\SeccionesEscuelas $seccion
     *
     * @return Block
     */
    public function setSeccion(\AdminBundle\Entity\SeccionesEscuelas $seccion = null)
    {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * Get seccion
     *
     * @return \AdminBundle\Entity\SeccionesEscuelas
     */
    public function getSeccion()
    {
        return $this->seccion;
    }
    /**
     * @var string
     */
    private $nameBlockOne;

    /**
     * @var string
     */
    private $contenBlockOne;

    /**
     * @var string
     */
    private $nameBlockSecond;

    /**
     * @var string
     */
    private $contenBlockSecond;

    /**
     * @var string
     */
    private $nameBlockThird;

    /**
     * @var string
     */
    private $contenBlockThird;


    /**
     * Set nameBlockOne
     *
     * @param string $nameBlockOne
     *
     * @return Block
     */
    public function setNameBlockOne($nameBlockOne)
    {
        $this->nameBlockOne = $nameBlockOne;

        return $this;
    }

    /**
     * Get nameBlockOne
     *
     * @return string
     */
    public function getNameBlockOne()
    {
        return $this->nameBlockOne;
    }

    /**
     * Set contenBlockOne
     *
     * @param string $contenBlockOne
     *
     * @return Block
     */
    public function setContenBlockOne($contenBlockOne)
    {
        $this->contenBlockOne = $contenBlockOne;

        return $this;
    }

    /**
     * Get contenBlockOne
     *
     * @return string
     */
    public function getContenBlockOne()
    {
        return $this->contenBlockOne;
    }

    /**
     * Set nameBlockSecond
     *
     * @param string $nameBlockSecond
     *
     * @return Block
     */
    public function setNameBlockSecond($nameBlockSecond)
    {
        $this->nameBlockSecond = $nameBlockSecond;

        return $this;
    }

    /**
     * Get nameBlockSecond
     *
     * @return string
     */
    public function getNameBlockSecond()
    {
        return $this->nameBlockSecond;
    }

    /**
     * Set contenBlockSecond
     *
     * @param string $contenBlockSecond
     *
     * @return Block
     */
    public function setContenBlockSecond($contenBlockSecond)
    {
        $this->contenBlockSecond = $contenBlockSecond;

        return $this;
    }

    /**
     * Get contenBlockSecond
     *
     * @return string
     */
    public function getContenBlockSecond()
    {
        return $this->contenBlockSecond;
    }

    /**
     * Set nameBlockThird
     *
     * @param string $nameBlockThird
     *
     * @return Block
     */
    public function setNameBlockThird($nameBlockThird)
    {
        $this->nameBlockThird = $nameBlockThird;

        return $this;
    }

    /**
     * Get nameBlockThird
     *
     * @return string
     */
    public function getNameBlockThird()
    {
        return $this->nameBlockThird;
    }

    /**
     * Set contenBlockThird
     *
     * @param string $contenBlockThird
     *
     * @return Block
     */
    public function setContenBlockThird($contenBlockThird)
    {
        $this->contenBlockThird = $contenBlockThird;

        return $this;
    }

    /**
     * Get contenBlockThird
     *
     * @return string
     */
    public function getContenBlockThird()
    {
        return $this->contenBlockThird;
    }
}
