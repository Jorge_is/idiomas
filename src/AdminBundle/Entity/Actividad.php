<?php

namespace AdminBundle\Entity;

/**
 * Actividad
 */
class Actividad
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \AdminBundle\Entity\Image
     */
    private $image;

    /**
     * @var \AdminBundle\Entity\CatEscuelas
     */
    private $escuela;


    /**
     * Set image
     *
     * @param \AdminBundle\Entity\Image $image
     *
     * @return Actividad
     */
    public function setImage(\AdminBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set escuela
     *
     * @param \AdminBundle\Entity\CatEscuelas $escuela
     *
     * @return Actividad
     */
    public function setEscuela(\AdminBundle\Entity\CatEscuelas $escuela = null)
    {
        $this->escuela = $escuela;

        return $this;
    }

    /**
     * Get escuela
     *
     * @return \AdminBundle\Entity\CatEscuelas
     */
    public function getEscuela()
    {
        return $this->escuela;
    }
}
