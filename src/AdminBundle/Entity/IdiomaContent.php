<?php

namespace AdminBundle\Entity;

/**
 * IdiomaContent
 */
class IdiomaContent
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $body;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return IdiomaContent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return IdiomaContent
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }
    /**
     * @var \AdminBundle\Entity\Idioma
     */
    private $idioma;


    /**
     * Set idioma
     *
     * @param \AdminBundle\Entity\Idioma $idioma
     *
     * @return IdiomaContent
     */
    public function setIdioma(\AdminBundle\Entity\Idioma $idioma = null)
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * Get idioma
     *
     * @return \AdminBundle\Entity\Idioma
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * @var \AdminBundle\Entity\Image
     */
    private $image;


    /**
     * Set image
     *
     * @param \AdminBundle\Entity\Image $image
     *
     * @return IdiomaContent
     */
    public function setImage(\AdminBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }
}
