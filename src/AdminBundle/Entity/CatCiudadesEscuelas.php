<?php

namespace AdminBundle\Entity;

/**
 * CatCiudadesEscuelas
 */
class CatCiudadesEscuelas
{
    /**
     * @var integer
     */
    private $idCiudad;

    /**
     * @v
     */
    private $ciudadDesc;

    /**
     * @var string
     */
    private $ciudadDescLarga;

    /**
     * @var integer
     */
    private $idPaisEdo;

    /**
     * @var integer
     */
    private $idEstadoEdo;

    /**
     * @var \AdminBundle\Entity\CatPaisesEscuelas
     */
    private $idPais;

    public function __toString() {
        return $this->getCiudadDesc();
    }


    /**
     * Get idCiudad
     *
     * @return integer
     */
    public function getIdCiudad()
    {
        return $this->idCiudad;
    }

    /**
     * Set ciudadDesc
     *
     * @param string $ciudadDesc
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDesc($ciudadDesc)
    {
        $this->ciudadDesc = $ciudadDesc;

        return $this;
    }

    /**
     * Get ciudadDesc
     *
     * @return string
     */
    public function getCiudadDesc()
    {
        return $this->ciudadDesc;
    }

    /**
     * Set ciudadDescLarga
     *
     * @param string $ciudadDescLarga
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDescLarga($ciudadDescLarga)
    {
        $this->ciudadDescLarga = $ciudadDescLarga;

        return $this;
    }

    /**
     * Get ciudadDescLarga
     *
     * @return string
     */
    public function getCiudadDescLarga()
    {
        return $this->ciudadDescLarga;
    }

    /**
     * Set idPaisEdo
     *
     * @param integer $idPaisEdo
     *
     * @return CatCiudadesEscuelas
     */
    public function setIdPaisEdo($idPaisEdo)
    {
        $this->idPaisEdo = $idPaisEdo;

        return $this;
    }

    /**
     * Get idPaisEdo
     *
     * @return integer
     */
    public function getIdPaisEdo()
    {
        return $this->idPaisEdo;
    }

    /**
     * Set idEstadoEdo
     *
     * @param integer $idEstadoEdo
     *
     * @return CatCiudadesEscuelas
     */
    public function setIdEstadoEdo($idEstadoEdo)
    {
        $this->idEstadoEdo = $idEstadoEdo;

        return $this;
    }

    /**
     * Get idEstadoEdo
     *
     * @return integer
     */
    public function getIdEstadoEdo()
    {
        return $this->idEstadoEdo;
    }


    /**
     * Set idPais
     *
     * @param \AdminBundle\Entity\CatPaisesEscuelas $idPais
     *
     * @return CatCiudadesEscuelas
     */
    public function setIdPais(\AdminBundle\Entity\CatPaisesEscuelas $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \AdminBundle\Entity\CatPaisesEscuelas
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * @var \AdminBundle\Entity\Image
     */
    private $imgBandera;

    /**
     * @var \AdminBundle\Entity\Image
     */
    private $imgModal;


    /**
     * Set imgBandera
     *
     * @param \AdminBundle\Entity\Image $imgBandera
     *
     * @return CatCiudadesEscuelas
     */
    public function setImgBandera(\AdminBundle\Entity\Image $imgBandera = null)
    {
        $this->imgBandera = $imgBandera;

        return $this;
    }

    /**
     * Get imgBandera
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getImgBandera()
    {
        return $this->imgBandera;
    }

    /**
     * Set imgModal
     *
     * @param \AdminBundle\Entity\Image $imgModal
     *
     * @return CatCiudadesEscuelas
     */
    public function setImgModal(\AdminBundle\Entity\Image $imgModal = null)
    {
        $this->imgModal = $imgModal;

        return $this;
    }

    /**
     * Get imgModal
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getImgModal()
    {
        return $this->imgModal;
    }
    /**
     * @var string
     */
    private $ciudad_desc_gen;

    /**
     * @var boolean
     */
    private $ciudad_tipo_comunidad;

    /**
     * @var integer
     */
    private $id_ciudad_prin;

    /**
     * @var integer
     */
    private $id_region_pais;

    /**
     * @var string
     */
    private $ciudad_desc_idi;

    /**
     * @var string
     */
    private $ciudad_desc_camp;

    /**
     * @var string
     */
    private $ciudad_desc_hs;

    /**
     * @var string
     */
    private $ciudad_desc_esup;

    /**
     * @var string
     */
    private $ciudad_desc_ws;

    /**
     * @var string
     */
    private $ciudad_ficha;

    /**
     * @var boolean
     */
    private $ciudad_ficha_npag_idi;

    /**
     * @var boolean
     */
    private $ciudad_ficha_npag_camp;

    /**
     * @var boolean
     */
    private $ciudad_ficha_npag_hs;

    /**
     * @var boolean
     */
    private $ciudad_ficha_npag_esup;

    /**
     * @var boolean
     */
    private $ciudad_ficha_npag_ws;


    /**
     * Set ciudadDescGen
     *
     * @param string $ciudadDescGen
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDescGen($ciudadDescGen)
    {
        $this->ciudad_desc_gen = $ciudadDescGen;

        return $this;
    }

    /**
     * Get ciudadDescGen
     *
     * @return string
     */
    public function getCiudadDescGen()
    {
        return $this->ciudad_desc_gen;
    }

    /**
     * Set ciudadTipoComunidad
     *
     * @param boolean $ciudadTipoComunidad
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadTipoComunidad($ciudadTipoComunidad)
    {
        $this->ciudad_tipo_comunidad = $ciudadTipoComunidad;

        return $this;
    }

    /**
     * Get ciudadTipoComunidad
     *
     * @return boolean
     */
    public function getCiudadTipoComunidad()
    {
        return $this->ciudad_tipo_comunidad;
    }

    /**
     * Set idCiudadPrin
     *
     * @param integer $idCiudadPrin
     *
     * @return CatCiudadesEscuelas
     */
    public function setIdCiudadPrin($idCiudadPrin)
    {
        $this->id_ciudad_prin = $idCiudadPrin;

        return $this;
    }

    /**
     * Get idCiudadPrin
     *
     * @return integer
     */
    public function getIdCiudadPrin()
    {
        return $this->id_ciudad_prin;
    }

    /**
     * Set idRegionPais
     *
     * @param integer $idRegionPais
     *
     * @return CatCiudadesEscuelas
     */
    public function setIdRegionPais($idRegionPais)
    {
        $this->id_region_pais = $idRegionPais;

        return $this;
    }

    /**
     * Get idRegionPais
     *
     * @return integer
     */
    public function getIdRegionPais()
    {
        return $this->id_region_pais;
    }

    /**
     * Set ciudadDescIdi
     *
     * @param string $ciudadDescIdi
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDescIdi($ciudadDescIdi)
    {
        $this->ciudad_desc_idi = $ciudadDescIdi;

        return $this;
    }

    /**
     * Get ciudadDescIdi
     *
     * @return string
     */
    public function getCiudadDescIdi()
    {
        return $this->ciudad_desc_idi;
    }

    /**
     * Set ciudadDescCamp
     *
     * @param string $ciudadDescCamp
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDescCamp($ciudadDescCamp)
    {
        $this->ciudad_desc_camp = $ciudadDescCamp;

        return $this;
    }

    /**
     * Get ciudadDescCamp
     *
     * @return string
     */
    public function getCiudadDescCamp()
    {
        return $this->ciudad_desc_camp;
    }

    /**
     * Set ciudadDescHs
     *
     * @param string $ciudadDescHs
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDescHs($ciudadDescHs)
    {
        $this->ciudad_desc_hs = $ciudadDescHs;

        return $this;
    }

    /**
     * Get ciudadDescHs
     *
     * @return string
     */
    public function getCiudadDescHs()
    {
        return $this->ciudad_desc_hs;
    }

    /**
     * Set ciudadDescEsup
     *
     * @param string $ciudadDescEsup
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDescEsup($ciudadDescEsup)
    {
        $this->ciudad_desc_esup = $ciudadDescEsup;

        return $this;
    }

    /**
     * Get ciudadDescEsup
     *
     * @return string
     */
    public function getCiudadDescEsup()
    {
        return $this->ciudad_desc_esup;
    }

    /**
     * Set ciudadDescWs
     *
     * @param string $ciudadDescWs
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadDescWs($ciudadDescWs)
    {
        $this->ciudad_desc_ws = $ciudadDescWs;

        return $this;
    }

    /**
     * Get ciudadDescWs
     *
     * @return string
     */
    public function getCiudadDescWs()
    {
        return $this->ciudad_desc_ws;
    }

    /**
     * Set ciudadFicha
     *
     * @param string $ciudadFicha
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadFicha($ciudadFicha)
    {
        $this->ciudad_ficha = $ciudadFicha;

        return $this;
    }

    /**
     * Get ciudadFicha
     *
     * @return string
     */
    public function getCiudadFicha()
    {
        return $this->ciudad_ficha;
    }

    /**
     * Set ciudadFichaNpagIdi
     *
     * @param boolean $ciudadFichaNpagIdi
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadFichaNpagIdi($ciudadFichaNpagIdi)
    {
        $this->ciudad_ficha_npag_idi = $ciudadFichaNpagIdi;

        return $this;
    }

    /**
     * Get ciudadFichaNpagIdi
     *
     * @return boolean
     */
    public function getCiudadFichaNpagIdi()
    {
        return $this->ciudad_ficha_npag_idi;
    }

    /**
     * Set ciudadFichaNpagCamp
     *
     * @param boolean $ciudadFichaNpagCamp
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadFichaNpagCamp($ciudadFichaNpagCamp)
    {
        $this->ciudad_ficha_npag_camp = $ciudadFichaNpagCamp;

        return $this;
    }

    /**
     * Get ciudadFichaNpagCamp
     *
     * @return boolean
     */
    public function getCiudadFichaNpagCamp()
    {
        return $this->ciudad_ficha_npag_camp;
    }

    /**
     * Set ciudadFichaNpagHs
     *
     * @param boolean $ciudadFichaNpagHs
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadFichaNpagHs($ciudadFichaNpagHs)
    {
        $this->ciudad_ficha_npag_hs = $ciudadFichaNpagHs;

        return $this;
    }

    /**
     * Get ciudadFichaNpagHs
     *
     * @return boolean
     */
    public function getCiudadFichaNpagHs()
    {
        return $this->ciudad_ficha_npag_hs;
    }

    /**
     * Set ciudadFichaNpagEsup
     *
     * @param boolean $ciudadFichaNpagEsup
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadFichaNpagEsup($ciudadFichaNpagEsup)
    {
        $this->ciudad_ficha_npag_esup = $ciudadFichaNpagEsup;

        return $this;
    }

    /**
     * Get ciudadFichaNpagEsup
     *
     * @return boolean
     */
    public function getCiudadFichaNpagEsup()
    {
        return $this->ciudad_ficha_npag_esup;
    }

    /**
     * Set ciudadFichaNpagWs
     *
     * @param boolean $ciudadFichaNpagWs
     *
     * @return CatCiudadesEscuelas
     */
    public function setCiudadFichaNpagWs($ciudadFichaNpagWs)
    {
        $this->ciudad_ficha_npag_ws = $ciudadFichaNpagWs;

        return $this;
    }

    /**
     * Get ciudadFichaNpagWs
     *
     * @return boolean
     */
    public function getCiudadFichaNpagWs()
    {
        return $this->ciudad_ficha_npag_ws;
    }

    /**
     * Set idCiudad
     *
     * @param integer $idCiudad
     *
     * @return CatCiudadesEscuelas
     */
    public function setIdCiudad($idCiudad)
    {
        $this->idCiudad = $idCiudad;

        return $this;
    }
}
