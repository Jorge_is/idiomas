<?php

namespace AdminBundle\Entity;

/**
 * CatEscuelas
 */
class CatEscuelas
{
    /**
     * @var integer
     */
    private $idEscuela;

    /**
     * @var string
     */
    private $escNombre;

    /**
     * @var string
     */
    private $escNombreCorto;



    public function __toString() {
        return $this->getEscNombre();
    }

    /**
     * Get idEscuela
     *
     * @return integer
     */
    public function getIdEscuela()
    {
        return $this->idEscuela;
    }

    /**
     * Set escNombre
     *
     * @param string $escNombre
     *
     * @return CatEscuelas
     */
    public function setEscNombre($escNombre)
    {
        $this->escNombre = $escNombre;

        return $this;
    }

    /**
     * Get escNombre
     *
     * @return string
     */
    public function getEscNombre()
    {
        return $this->escNombre;
    }

    /**
     * Set escNombreCorto
     *
     * @param string $escNombreCorto
     *
     * @return CatEscuelas
     */
    public function setEscNombreCorto($escNombreCorto)
    {
        $this->escNombreCorto = $escNombreCorto;

        return $this;
    }

    /**
     * Get escNombreCorto
     *
     * @return string
     */
    public function getEscNombreCorto()
    {
        return $this->escNombreCorto;
    }
    
    /**
     * @var \AdminBundle\Entity\CatPaisesEscuelas
     */
    private $idPais;

    /**
     * @var \AdminBundle\Entity\CatCiudadesEscuelas
     */
    private $idCiudad;


    /**
     * Set idPais
     *
     * @param \AdminBundle\Entity\CatPaisesEscuelas $idPais
     *
     * @return CatEscuelas
     */
    public function setIdPais(\AdminBundle\Entity\CatPaisesEscuelas $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \AdminBundle\Entity\CatPaisesEscuelas
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * Set idCiudad
     *
     * @param \AdminBundle\Entity\CatCiudadesEscuelas $idCiudad
     *
     * @return CatEscuelas
     */
    public function setIdCiudad(\AdminBundle\Entity\CatCiudadesEscuelas $idCiudad = null)
    {
        $this->idCiudad = $idCiudad;

        return $this;
    }

    /**
     * Get idCiudad
     *
     * @return \AdminBundle\Entity\CatCiudadesEscuelas
     */
    public function getIdCiudad()
    {
        return $this->idCiudad;
    }

    /**
     * @var \AdminBundle\Entity\Image
     */
    private $esc19;

    /**
     * @var \AdminBundle\Entity\Image
     */
    private $banner;


    /**
     * Set esc19
     *
     * @param \AdminBundle\Entity\Image $esc19
     *
     * @return CatEscuelas
     */
    public function setEsc19(\AdminBundle\Entity\Image $esc19 = null)
    {
        $this->esc19 = $esc19;

        return $this;
    }

    /**
     * Get esc19
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getEsc19()
    {
        return $this->esc19;
    }

    /**
     * Set banner
     *
     * @param \AdminBundle\Entity\Image $banner
     *
     * @return CatEscuelas
     */
    public function setBanner(\AdminBundle\Entity\Image $banner = null)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return \AdminBundle\Entity\Image
     */
    public function getBanner()
    {
        return $this->banner;
    }
    /**
     * @var boolean
     */
    private $idMoneda;


    /**
     * Set idMoneda
     *
     * @param boolean $idMoneda
     *
     * @return CatEscuelas
     */
    public function setIdMoneda($idMoneda)
    {
        $this->idMoneda = $idMoneda;

        return $this;
    }

    /**
     * Get idMoneda
     *
     * @return boolean
     */
    public function getIdMoneda()
    {
        return $this->idMoneda;
    }
    /**
     * @var string
     */
    private $esc_ficha_tecnica;

    /**
     * @var string
     */
    private $esc_desc;


    /**
     * Set escFichaTecnica
     *
     * @param string $escFichaTecnica
     *
     * @return CatEscuelas
     */
    public function setEscFichaTecnica($escFichaTecnica)
    {
        $this->esc_ficha_tecnica = $escFichaTecnica;

        return $this;
    }

    /**
     * Get escFichaTecnica
     *
     * @return string
     */
    public function getEscFichaTecnica()
    {
        return $this->esc_ficha_tecnica;
    }

    /**
     * Set escDesc
     *
     * @param string $escDesc
     *
     * @return CatEscuelas
     */
    public function setEscDesc($escDesc)
    {
        $this->esc_desc = $escDesc;

        return $this;
    }

    /**
     * Get escDesc
     *
     * @return string
     */
    public function getEscDesc()
    {
        return $this->esc_desc;
    }
    /**
     * @var boolean
     */
    private $esc_tamano;

    /**
     * @var boolean
     */
    private $esc_num_salones;

    /**
     * @var boolean
     */
    private $esc_tipo;

    /**
     * @var boolean
     */
    private $esc_ib;

    /**
     * @var boolean
     */
    private $esc_clasif;

    /**
     * @var boolean
     */
    private $esc_generos;

    /**
     * @var boolean
     */
    private $esc_cre;

    /**
     * @var boolean
     */
    private $esc_estatus;

    /**
     * @var boolean
     */
    private $esc_estatus_expo;

    /**
     * @var integer
     */
    private $id_distrito;

    /**
     * @var boolean
     */
    private $id_grupo_esup;

    /**
     * @var float
     */
    private $id_pond_idi;

    /**
     * @var float
     */
    private $id_pond_camp;

    /**
     * @var float
     */
    private $id_pond_hs;

    /**
     * @var float
     */
    private $id_pond_esup;

    /**
     * @var float
     */
    private $id_pond_ws;

    /**
     * @var string
     */
    private $esc_logo;

    /**
     * @var integer
     */
    private $id_aerop;

    /**
     * @var float
     */
    private $esc_anio_comis;

    /**
     * @var boolean
     */
    private $esc_hs_normal;

    /**
     * @var boolean
     */
    private $esc_hs_honors;

    /**
     * @var boolean
     */
    private $esc_hs_inter;

    /**
     * @var boolean
     */
    private $esc_hs_tipo;

    /**
     * @var boolean
     */
    private $esc_ap;

    /**
     * @var boolean
     */
    private $esc_inmersion_idi;

    /**
     * @var string
     */
    private $esc_ficha;

    /**
     * @var boolean
     */
    private $esc_ficha_npag;

    /**
     * @var boolean
     */
    private $id_programa;


    /**
     * Set escTamano
     *
     * @param boolean $escTamano
     *
     * @return CatEscuelas
     */
    public function setEscTamano($escTamano)
    {
        $this->esc_tamano = $escTamano;

        return $this;
    }

    /**
     * Get escTamano
     *
     * @return boolean
     */
    public function getEscTamano()
    {
        return $this->esc_tamano;
    }

    /**
     * Set escNumSalones
     *
     * @param boolean $escNumSalones
     *
     * @return CatEscuelas
     */
    public function setEscNumSalones($escNumSalones)
    {
        $this->esc_num_salones = $escNumSalones;

        return $this;
    }

    /**
     * Get escNumSalones
     *
     * @return boolean
     */
    public function getEscNumSalones()
    {
        return $this->esc_num_salones;
    }

    /**
     * Set escTipo
     *
     * @param boolean $escTipo
     *
     * @return CatEscuelas
     */
    public function setEscTipo($escTipo)
    {
        $this->esc_tipo = $escTipo;

        return $this;
    }

    /**
     * Get escTipo
     *
     * @return boolean
     */
    public function getEscTipo()
    {
        return $this->esc_tipo;
    }

    /**
     * Set escIb
     *
     * @param boolean $escIb
     *
     * @return CatEscuelas
     */
    public function setEscIb($escIb)
    {
        $this->esc_ib = $escIb;

        return $this;
    }

    /**
     * Get escIb
     *
     * @return boolean
     */
    public function getEscIb()
    {
        return $this->esc_ib;
    }

    /**
     * Set escClasif
     *
     * @param boolean $escClasif
     *
     * @return CatEscuelas
     */
    public function setEscClasif($escClasif)
    {
        $this->esc_clasif = $escClasif;

        return $this;
    }

    /**
     * Get escClasif
     *
     * @return boolean
     */
    public function getEscClasif()
    {
        return $this->esc_clasif;
    }

    /**
     * Set escGeneros
     *
     * @param boolean $escGeneros
     *
     * @return CatEscuelas
     */
    public function setEscGeneros($escGeneros)
    {
        $this->esc_generos = $escGeneros;

        return $this;
    }

    /**
     * Get escGeneros
     *
     * @return boolean
     */
    public function getEscGeneros()
    {
        return $this->esc_generos;
    }

    /**
     * Set escCre
     *
     * @param boolean $escCre
     *
     * @return CatEscuelas
     */
    public function setEscCre($escCre)
    {
        $this->esc_cre = $escCre;

        return $this;
    }

    /**
     * Get escCre
     *
     * @return boolean
     */
    public function getEscCre()
    {
        return $this->esc_cre;
    }

    /**
     * Set escEstatus
     *
     * @param boolean $escEstatus
     *
     * @return CatEscuelas
     */
    public function setEscEstatus($escEstatus)
    {
        $this->esc_estatus = $escEstatus;

        return $this;
    }

    /**
     * Get escEstatus
     *
     * @return boolean
     */
    public function getEscEstatus()
    {
        return $this->esc_estatus;
    }

    /**
     * Set escEstatusExpo
     *
     * @param boolean $escEstatusExpo
     *
     * @return CatEscuelas
     */
    public function setEscEstatusExpo($escEstatusExpo)
    {
        $this->esc_estatus_expo = $escEstatusExpo;

        return $this;
    }

    /**
     * Get escEstatusExpo
     *
     * @return boolean
     */
    public function getEscEstatusExpo()
    {
        return $this->esc_estatus_expo;
    }

    /**
     * Set idDistrito
     *
     * @param integer $idDistrito
     *
     * @return CatEscuelas
     */
    public function setIdDistrito($idDistrito)
    {
        $this->id_distrito = $idDistrito;

        return $this;
    }

    /**
     * Get idDistrito
     *
     * @return integer
     */
    public function getIdDistrito()
    {
        return $this->id_distrito;
    }

    /**
     * Set idGrupoEsup
     *
     * @param boolean $idGrupoEsup
     *
     * @return CatEscuelas
     */
    public function setIdGrupoEsup($idGrupoEsup)
    {
        $this->id_grupo_esup = $idGrupoEsup;

        return $this;
    }

    /**
     * Get idGrupoEsup
     *
     * @return boolean
     */
    public function getIdGrupoEsup()
    {
        return $this->id_grupo_esup;
    }

    /**
     * Set idPondIdi
     *
     * @param float $idPondIdi
     *
     * @return CatEscuelas
     */
    public function setIdPondIdi($idPondIdi)
    {
        $this->id_pond_idi = $idPondIdi;

        return $this;
    }

    /**
     * Get idPondIdi
     *
     * @return float
     */
    public function getIdPondIdi()
    {
        return $this->id_pond_idi;
    }

    /**
     * Set idPondCamp
     *
     * @param float $idPondCamp
     *
     * @return CatEscuelas
     */
    public function setIdPondCamp($idPondCamp)
    {
        $this->id_pond_camp = $idPondCamp;

        return $this;
    }

    /**
     * Get idPondCamp
     *
     * @return float
     */
    public function getIdPondCamp()
    {
        return $this->id_pond_camp;
    }

    /**
     * Set idPondHs
     *
     * @param float $idPondHs
     *
     * @return CatEscuelas
     */
    public function setIdPondHs($idPondHs)
    {
        $this->id_pond_hs = $idPondHs;

        return $this;
    }

    /**
     * Get idPondHs
     *
     * @return float
     */
    public function getIdPondHs()
    {
        return $this->id_pond_hs;
    }

    /**
     * Set idPondEsup
     *
     * @param float $idPondEsup
     *
     * @return CatEscuelas
     */
    public function setIdPondEsup($idPondEsup)
    {
        $this->id_pond_esup = $idPondEsup;

        return $this;
    }

    /**
     * Get idPondEsup
     *
     * @return float
     */
    public function getIdPondEsup()
    {
        return $this->id_pond_esup;
    }

    /**
     * Set idPondWs
     *
     * @param float $idPondWs
     *
     * @return CatEscuelas
     */
    public function setIdPondWs($idPondWs)
    {
        $this->id_pond_ws = $idPondWs;

        return $this;
    }

    /**
     * Get idPondWs
     *
     * @return float
     */
    public function getIdPondWs()
    {
        return $this->id_pond_ws;
    }

    /**
     * Set escLogo
     *
     * @param string $escLogo
     *
     * @return CatEscuelas
     */
    public function setEscLogo($escLogo)
    {
        $this->esc_logo = $escLogo;

        return $this;
    }

    /**
     * Get escLogo
     *
     * @return string
     */
    public function getEscLogo()
    {
        return $this->esc_logo;
    }

    /**
     * Set idAerop
     *
     * @param integer $idAerop
     *
     * @return CatEscuelas
     */
    public function setIdAerop($idAerop)
    {
        $this->id_aerop = $idAerop;

        return $this;
    }

    /**
     * Get idAerop
     *
     * @return integer
     */
    public function getIdAerop()
    {
        return $this->id_aerop;
    }

    /**
     * Set escAnioComis
     *
     * @param float $escAnioComis
     *
     * @return CatEscuelas
     */
    public function setEscAnioComis($escAnioComis)
    {
        $this->esc_anio_comis = $escAnioComis;

        return $this;
    }

    /**
     * Get escAnioComis
     *
     * @return float
     */
    public function getEscAnioComis()
    {
        return $this->esc_anio_comis;
    }

    /**
     * Set escHsNormal
     *
     * @param boolean $escHsNormal
     *
     * @return CatEscuelas
     */
    public function setEscHsNormal($escHsNormal)
    {
        $this->esc_hs_normal = $escHsNormal;

        return $this;
    }

    /**
     * Get escHsNormal
     *
     * @return boolean
     */
    public function getEscHsNormal()
    {
        return $this->esc_hs_normal;
    }

    /**
     * Set escHsHonors
     *
     * @param boolean $escHsHonors
     *
     * @return CatEscuelas
     */
    public function setEscHsHonors($escHsHonors)
    {
        $this->esc_hs_honors = $escHsHonors;

        return $this;
    }

    /**
     * Get escHsHonors
     *
     * @return boolean
     */
    public function getEscHsHonors()
    {
        return $this->esc_hs_honors;
    }

    /**
     * Set escHsInter
     *
     * @param boolean $escHsInter
     *
     * @return CatEscuelas
     */
    public function setEscHsInter($escHsInter)
    {
        $this->esc_hs_inter = $escHsInter;

        return $this;
    }

    /**
     * Get escHsInter
     *
     * @return boolean
     */
    public function getEscHsInter()
    {
        return $this->esc_hs_inter;
    }

    /**
     * Set escHsTipo
     *
     * @param boolean $escHsTipo
     *
     * @return CatEscuelas
     */
    public function setEscHsTipo($escHsTipo)
    {
        $this->esc_hs_tipo = $escHsTipo;

        return $this;
    }

    /**
     * Get escHsTipo
     *
     * @return boolean
     */
    public function getEscHsTipo()
    {
        return $this->esc_hs_tipo;
    }

    /**
     * Set escAp
     *
     * @param boolean $escAp
     *
     * @return CatEscuelas
     */
    public function setEscAp($escAp)
    {
        $this->esc_ap = $escAp;

        return $this;
    }

    /**
     * Get escAp
     *
     * @return boolean
     */
    public function getEscAp()
    {
        return $this->esc_ap;
    }

    /**
     * Set escInmersionIdi
     *
     * @param boolean $escInmersionIdi
     *
     * @return CatEscuelas
     */
    public function setEscInmersionIdi($escInmersionIdi)
    {
        $this->esc_inmersion_idi = $escInmersionIdi;

        return $this;
    }

    /**
     * Get escInmersionIdi
     *
     * @return boolean
     */
    public function getEscInmersionIdi()
    {
        return $this->esc_inmersion_idi;
    }

    /**
     * Set escFicha
     *
     * @param string $escFicha
     *
     * @return CatEscuelas
     */
    public function setEscFicha($escFicha)
    {
        $this->esc_ficha = $escFicha;

        return $this;
    }

    /**
     * Get escFicha
     *
     * @return string
     */
    public function getEscFicha()
    {
        return $this->esc_ficha;
    }

    /**
     * Set escFichaNpag
     *
     * @param boolean $escFichaNpag
     *
     * @return CatEscuelas
     */
    public function setEscFichaNpag($escFichaNpag)
    {
        $this->esc_ficha_npag = $escFichaNpag;

        return $this;
    }

    /**
     * Get escFichaNpag
     *
     * @return boolean
     */
    public function getEscFichaNpag()
    {
        return $this->esc_ficha_npag;
    }

    /**
     * Set idPrograma
     *
     * @param boolean $idPrograma
     *
     * @return CatEscuelas
     */
    public function setIdPrograma($idPrograma)
    {
        $this->id_programa = $idPrograma;

        return $this;
    }

    /**
     * Get idPrograma
     *
     * @return boolean
     */
    public function getIdPrograma()
    {
        return $this->id_programa;
    }
    /**
     * @var string
     */
    private $correo_idi;

    /**
     * @var string
     */
    private $correo_idi_cc;


    /**
     * Set correoIdi
     *
     * @param string $correoIdi
     *
     * @return CatEscuelas
     */
    public function setCorreoIdi($correoIdi)
    {
        $this->correo_idi = $correoIdi;

        return $this;
    }

    /**
     * Get correoIdi
     *
     * @return string
     */
    public function getCorreoIdi()
    {
        return $this->correo_idi;
    }

    /**
     * Set correoIdiCc
     *
     * @param string $correoIdiCc
     *
     * @return CatEscuelas
     */
    public function setCorreoIdiCc($correoIdiCc)
    {
        $this->correo_idi_cc = $correoIdiCc;

        return $this;
    }

    /**
     * Get correoIdiCc
     *
     * @return string
     */
    public function getCorreoIdiCc()
    {
        return $this->correo_idi_cc;
    }
    /**
     * @var string
     */
    private $correo_idi_aloj;

    /**
     * @var string
     */
    private $correo_camp;

    /**
     * @var string
     */
    private $correo_camp_cc;

    /**
     * @var string
     */
    private $correo_camp_aloj;

    /**
     * @var string
     */
    private $correo_hs;

    /**
     * @var string
     */
    private $correo_hs_cc;

    /**
     * @var string
     */
    private $correo_hs_aloj;

    /**
     * @var string
     */
    private $correo_edu;

    /**
     * @var string
     */
    private $correo_edu_cc;

    /**
     * @var string
     */
    private $correo_edu_aloj;


    /**
     * Set correoIdiAloj
     *
     * @param string $correoIdiAloj
     *
     * @return CatEscuelas
     */
    public function setCorreoIdiAloj($correoIdiAloj)
    {
        $this->correo_idi_aloj = $correoIdiAloj;

        return $this;
    }

    /**
     * Get correoIdiAloj
     *
     * @return string
     */
    public function getCorreoIdiAloj()
    {
        return $this->correo_idi_aloj;
    }

    /**
     * Set correoCamp
     *
     * @param string $correoCamp
     *
     * @return CatEscuelas
     */
    public function setCorreoCamp($correoCamp)
    {
        $this->correo_camp = $correoCamp;

        return $this;
    }

    /**
     * Get correoCamp
     *
     * @return string
     */
    public function getCorreoCamp()
    {
        return $this->correo_camp;
    }

    /**
     * Set correoCampCc
     *
     * @param string $correoCampCc
     *
     * @return CatEscuelas
     */
    public function setCorreoCampCc($correoCampCc)
    {
        $this->correo_camp_cc = $correoCampCc;

        return $this;
    }

    /**
     * Get correoCampCc
     *
     * @return string
     */
    public function getCorreoCampCc()
    {
        return $this->correo_camp_cc;
    }

    /**
     * Set correoCampAloj
     *
     * @param string $correoCampAloj
     *
     * @return CatEscuelas
     */
    public function setCorreoCampAloj($correoCampAloj)
    {
        $this->correo_camp_aloj = $correoCampAloj;

        return $this;
    }

    /**
     * Get correoCampAloj
     *
     * @return string
     */
    public function getCorreoCampAloj()
    {
        return $this->correo_camp_aloj;
    }

    /**
     * Set correoHs
     *
     * @param string $correoHs
     *
     * @return CatEscuelas
     */
    public function setCorreoHs($correoHs)
    {
        $this->correo_hs = $correoHs;

        return $this;
    }

    /**
     * Get correoHs
     *
     * @return string
     */
    public function getCorreoHs()
    {
        return $this->correo_hs;
    }

    /**
     * Set correoHsCc
     *
     * @param string $correoHsCc
     *
     * @return CatEscuelas
     */
    public function setCorreoHsCc($correoHsCc)
    {
        $this->correo_hs_cc = $correoHsCc;

        return $this;
    }

    /**
     * Get correoHsCc
     *
     * @return string
     */
    public function getCorreoHsCc()
    {
        return $this->correo_hs_cc;
    }

    /**
     * Set correoHsAloj
     *
     * @param string $correoHsAloj
     *
     * @return CatEscuelas
     */
    public function setCorreoHsAloj($correoHsAloj)
    {
        $this->correo_hs_aloj = $correoHsAloj;

        return $this;
    }

    /**
     * Get correoHsAloj
     *
     * @return string
     */
    public function getCorreoHsAloj()
    {
        return $this->correo_hs_aloj;
    }

    /**
     * Set correoEdu
     *
     * @param string $correoEdu
     *
     * @return CatEscuelas
     */
    public function setCorreoEdu($correoEdu)
    {
        $this->correo_edu = $correoEdu;

        return $this;
    }

    /**
     * Get correoEdu
     *
     * @return string
     */
    public function getCorreoEdu()
    {
        return $this->correo_edu;
    }

    /**
     * Set correoEduCc
     *
     * @param string $correoEduCc
     *
     * @return CatEscuelas
     */
    public function setCorreoEduCc($correoEduCc)
    {
        $this->correo_edu_cc = $correoEduCc;

        return $this;
    }

    /**
     * Get correoEduCc
     *
     * @return string
     */
    public function getCorreoEduCc()
    {
        return $this->correo_edu_cc;
    }

    /**
     * Set correoEduAloj
     *
     * @param string $correoEduAloj
     *
     * @return CatEscuelas
     */
    public function setCorreoEduAloj($correoEduAloj)
    {
        $this->correo_edu_aloj = $correoEduAloj;

        return $this;
    }

    /**
     * Get correoEduAloj
     *
     * @return string
     */
    public function getCorreoEduAloj()
    {
        return $this->correo_edu_aloj;
    }

    /**
     * Set idEscuela
     *
     * @param integer $idEscuela
     *
     * @return CatEscuelas
     */
    public function setIdEscuela($idEscuela)
    {
        $this->idEscuela = $idEscuela;

        return $this;
    }
}
