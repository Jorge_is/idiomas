<?php

namespace AdminBundle\Entity;

/**
 * Photogallery
 */
class Photogallery
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \AdminBundle\Entity\CatEscuelas
     */
    private $escuela;

    /**
     * Set escuela
     *
     * @param \AdminBundle\Entity\CatEscuelas $escuela
     *
     * @return Photogallery
     */
    public function setEscuela(\AdminBundle\Entity\CatEscuelas $escuela = null)
    {
        $this->escuela = $escuela;

        return $this;
    }

    /**
     * Get escuela
     *
     * @return \AdminBundle\Entity\CatEscuelas
     */
    public function getEscuela()
    {
        return $this->escuela;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $image;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image
     *
     * @param \AdminBundle\Entity\Image $image
     *
     * @return Photogallery
     */
    public function addImage(\AdminBundle\Entity\Image $image)
    {
        $this->image[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AdminBundle\Entity\Image $image
     */
    public function removeImage(\AdminBundle\Entity\Image $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $video;


    /**
     * Add video
     *
     * @param \AdminBundle\Entity\Videos $video
     *
     * @return Photogallery
     */
    public function addVideo(\AdminBundle\Entity\Videos $video)
    {
        $this->video[] = $video;

        return $this;
    }

    /**
     * Remove video
     *
     * @param \AdminBundle\Entity\Videos $video
     */
    public function removeVideo(\AdminBundle\Entity\Videos $video)
    {
        $this->video->removeElement($video);
    }

    /**
     * Get video
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideo()
    {
        return $this->video;
    }
}
