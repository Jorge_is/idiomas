<?php

namespace AdminBundle\Entity;

/**
 * RegEscuelasCursos
 */
class RegEscuelasCursos
{
    /**
     * @var integer
     */
    private $idCursoD;

    /**
     * @var integer
     */
    private $idIdioma;

    /**
     * @var string
     */
    private $escCurNombre;

    /**
     * @var string
     */
    private $escCurNombreIngles;

    /**
     * @var string
     */
    private $escCurNombreCorto;

    /**
     * @var float
     */
    private $escCurEpsProm;

    /**
     * @var float
     */
    private $escCurEpsMax;

    /**
     * @var float
     */
    private $escCurNivNum;

    /**
     * @var string
     */
    private $escCurNivDuraSem;

    /**
     * @var float
     */
    private $escCurEdadMin;

    /**
     * @var float
     */
    private $escCurEdadMax;

    /**
     * @var float
     */
    private $escCurEdadRecom;

    /**
     * @var string
     */
    private $escCurNivMin;

    /**
     * @var boolean
     */
    private $escCurNivMinReq;

    /**
     * @var float
     */
    private $escCurEstudMin;

    /**
     * @var float
     */
    private $escCurDuraMinSem;

    /**
     * @var string
     */
    private $escCurDuraMinTipo;

    /**
     * @var float
     */
    private $escCurDuraSem;

    /**
     * @var string
     */
    private $escCurDuraTipo;

    /**
     * @var string
     */
    private $escCurInicia;

    /**
     * @var float
     */
    private $escCurLecDuraMinu;

    /**
     * @var integer
     */
    private $escGrado;

    /**
     * @var integer
     */
    private $escArea;

    /**
     * @var string
     */
    private $escCurDesc;

    /**
     * @var integer
     */
    private $escCurInteres;

    /**
     * @var integer
     */
    private $escCurInteresDet;

    /**
     * @var integer
     */
    private $idCiudadPrin;

    /**
     * @var string
     */
    private $escCurPromo1;

    /**
     * @var string
     */
    private $escCurPromo2;

    /**
     * @var string
     */
    private $escCurCampus;

    /**
     * @var boolean
     */
    private $escCurNivelSe;

    /**
     * @var boolean
     */
    private $escCurGrupos;

    /**
     * @var string
     */
    private $escCurRequisitos;

    /**
     * @var string
     */
    private $escCurMaterias;

    /**
     * @var string
     */
    private $escCurAreasTrabajo;

    /**
     * @var boolean
     */
    private $escCurEstatus;

    /**
     * @var boolean
     */
    private $escCurInmersionIdi;

    /**
     * @var integer
     */
    private $idCarga;

    /**
     * @var string
     */
    private $escFicha;

    /**
     * @var boolean
     */
    private $escFichaNpag;

    /**
     * @var boolean
     */
    private $escVideos;

    /**
     * @var \AdminBundle\Entity\CatCursos
     */
    private $idCurso;

    /**
     * @var \AdminBundle\Entity\CatEscuelas
     */
    private $idEscuela;


    /**
     * Set idCursoD
     *
     * @param integer $idCursoD
     *
     * @return RegEscuelasCursos
     */
    public function setIdCursoD($idCursoD)
    {
        $this->idCursoD = $idCursoD;

        return $this;
    }

    /**
     * Get idCursoD
     *
     * @return integer
     */
    public function getIdCursoD()
    {
        return $this->idCursoD;
    }

    /**
     * Set idIdioma
     *
     * @param integer $idIdioma
     *
     * @return RegEscuelasCursos
     */
    public function setIdIdioma($idIdioma)
    {
        $this->idIdioma = $idIdioma;

        return $this;
    }

    /**
     * Get idIdioma
     *
     * @return integer
     */
    public function getIdIdioma()
    {
        return $this->idIdioma;
    }

    /**
     * Set escCurNombre
     *
     * @param string $escCurNombre
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNombre($escCurNombre)
    {
        $this->escCurNombre = $escCurNombre;

        return $this;
    }

    /**
     * Get escCurNombre
     *
     * @return string
     */
    public function getEscCurNombre()
    {
        return $this->escCurNombre;
    }

    /**
     * Set escCurNombreIngles
     *
     * @param string $escCurNombreIngles
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNombreIngles($escCurNombreIngles)
    {
        $this->escCurNombreIngles = $escCurNombreIngles;

        return $this;
    }

    /**
     * Get escCurNombreIngles
     *
     * @return string
     */
    public function getEscCurNombreIngles()
    {
        return $this->escCurNombreIngles;
    }

    /**
     * Set escCurNombreCorto
     *
     * @param string $escCurNombreCorto
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNombreCorto($escCurNombreCorto)
    {
        $this->escCurNombreCorto = $escCurNombreCorto;

        return $this;
    }

    /**
     * Get escCurNombreCorto
     *
     * @return string
     */
    public function getEscCurNombreCorto()
    {
        return $this->escCurNombreCorto;
    }

    /**
     * Set escCurEpsProm
     *
     * @param float $escCurEpsProm
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurEpsProm($escCurEpsProm)
    {
        $this->escCurEpsProm = $escCurEpsProm;

        return $this;
    }

    /**
     * Get escCurEpsProm
     *
     * @return float
     */
    public function getEscCurEpsProm()
    {
        return $this->escCurEpsProm;
    }

    /**
     * Set escCurEpsMax
     *
     * @param float $escCurEpsMax
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurEpsMax($escCurEpsMax)
    {
        $this->escCurEpsMax = $escCurEpsMax;

        return $this;
    }

    /**
     * Get escCurEpsMax
     *
     * @return float
     */
    public function getEscCurEpsMax()
    {
        return $this->escCurEpsMax;
    }

    /**
     * Set escCurNivNum
     *
     * @param float $escCurNivNum
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNivNum($escCurNivNum)
    {
        $this->escCurNivNum = $escCurNivNum;

        return $this;
    }

    /**
     * Get escCurNivNum
     *
     * @return float
     */
    public function getEscCurNivNum()
    {
        return $this->escCurNivNum;
    }

    /**
     * Set escCurNivDuraSem
     *
     * @param string $escCurNivDuraSem
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNivDuraSem($escCurNivDuraSem)
    {
        $this->escCurNivDuraSem = $escCurNivDuraSem;

        return $this;
    }

    /**
     * Get escCurNivDuraSem
     *
     * @return string
     */
    public function getEscCurNivDuraSem()
    {
        return $this->escCurNivDuraSem;
    }

    /**
     * Set escCurEdadMin
     *
     * @param float $escCurEdadMin
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurEdadMin($escCurEdadMin)
    {
        $this->escCurEdadMin = $escCurEdadMin;

        return $this;
    }

    /**
     * Get escCurEdadMin
     *
     * @return float
     */
    public function getEscCurEdadMin()
    {
        return $this->escCurEdadMin;
    }

    /**
     * Set escCurEdadMax
     *
     * @param float $escCurEdadMax
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurEdadMax($escCurEdadMax)
    {
        $this->escCurEdadMax = $escCurEdadMax;

        return $this;
    }

    /**
     * Get escCurEdadMax
     *
     * @return float
     */
    public function getEscCurEdadMax()
    {
        return $this->escCurEdadMax;
    }

    /**
     * Set escCurEdadRecom
     *
     * @param float $escCurEdadRecom
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurEdadRecom($escCurEdadRecom)
    {
        $this->escCurEdadRecom = $escCurEdadRecom;

        return $this;
    }

    /**
     * Get escCurEdadRecom
     *
     * @return float
     */
    public function getEscCurEdadRecom()
    {
        return $this->escCurEdadRecom;
    }

    /**
     * Set escCurNivMin
     *
     * @param string $escCurNivMin
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNivMin($escCurNivMin)
    {
        $this->escCurNivMin = $escCurNivMin;

        return $this;
    }

    /**
     * Get escCurNivMin
     *
     * @return string
     */
    public function getEscCurNivMin()
    {
        return $this->escCurNivMin;
    }

    /**
     * Set escCurNivMinReq
     *
     * @param boolean $escCurNivMinReq
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNivMinReq($escCurNivMinReq)
    {
        $this->escCurNivMinReq = $escCurNivMinReq;

        return $this;
    }

    /**
     * Get escCurNivMinReq
     *
     * @return boolean
     */
    public function getEscCurNivMinReq()
    {
        return $this->escCurNivMinReq;
    }

    /**
     * Set escCurEstudMin
     *
     * @param float $escCurEstudMin
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurEstudMin($escCurEstudMin)
    {
        $this->escCurEstudMin = $escCurEstudMin;

        return $this;
    }

    /**
     * Get escCurEstudMin
     *
     * @return float
     */
    public function getEscCurEstudMin()
    {
        return $this->escCurEstudMin;
    }

    /**
     * Set escCurDuraMinSem
     *
     * @param float $escCurDuraMinSem
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurDuraMinSem($escCurDuraMinSem)
    {
        $this->escCurDuraMinSem = $escCurDuraMinSem;

        return $this;
    }

    /**
     * Get escCurDuraMinSem
     *
     * @return float
     */
    public function getEscCurDuraMinSem()
    {
        return $this->escCurDuraMinSem;
    }

    /**
     * Set escCurDuraMinTipo
     *
     * @param string $escCurDuraMinTipo
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurDuraMinTipo($escCurDuraMinTipo)
    {
        $this->escCurDuraMinTipo = $escCurDuraMinTipo;

        return $this;
    }

    /**
     * Get escCurDuraMinTipo
     *
     * @return string
     */
    public function getEscCurDuraMinTipo()
    {
        return $this->escCurDuraMinTipo;
    }

    /**
     * Set escCurDuraSem
     *
     * @param float $escCurDuraSem
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurDuraSem($escCurDuraSem)
    {
        $this->escCurDuraSem = $escCurDuraSem;

        return $this;
    }

    /**
     * Get escCurDuraSem
     *
     * @return float
     */
    public function getEscCurDuraSem()
    {
        return $this->escCurDuraSem;
    }

    /**
     * Set escCurDuraTipo
     *
     * @param string $escCurDuraTipo
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurDuraTipo($escCurDuraTipo)
    {
        $this->escCurDuraTipo = $escCurDuraTipo;

        return $this;
    }

    /**
     * Get escCurDuraTipo
     *
     * @return string
     */
    public function getEscCurDuraTipo()
    {
        return $this->escCurDuraTipo;
    }

    /**
     * Set escCurInicia
     *
     * @param string $escCurInicia
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurInicia($escCurInicia)
    {
        $this->escCurInicia = $escCurInicia;

        return $this;
    }

    /**
     * Get escCurInicia
     *
     * @return string
     */
    public function getEscCurInicia()
    {
        return $this->escCurInicia;
    }

    /**
     * Set escCurLecDuraMinu
     *
     * @param float $escCurLecDuraMinu
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurLecDuraMinu($escCurLecDuraMinu)
    {
        $this->escCurLecDuraMinu = $escCurLecDuraMinu;

        return $this;
    }

    /**
     * Get escCurLecDuraMinu
     *
     * @return float
     */
    public function getEscCurLecDuraMinu()
    {
        return $this->escCurLecDuraMinu;
    }

    /**
     * Set escGrado
     *
     * @param integer $escGrado
     *
     * @return RegEscuelasCursos
     */
    public function setEscGrado($escGrado)
    {
        $this->escGrado = $escGrado;

        return $this;
    }

    /**
     * Get escGrado
     *
     * @return integer
     */
    public function getEscGrado()
    {
        return $this->escGrado;
    }

    /**
     * Set escArea
     *
     * @param integer $escArea
     *
     * @return RegEscuelasCursos
     */
    public function setEscArea($escArea)
    {
        $this->escArea = $escArea;

        return $this;
    }

    /**
     * Get escArea
     *
     * @return integer
     */
    public function getEscArea()
    {
        return $this->escArea;
    }

    /**
     * Set escCurDesc
     *
     * @param string $escCurDesc
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurDesc($escCurDesc)
    {
        $this->escCurDesc = $escCurDesc;

        return $this;
    }

    /**
     * Get escCurDesc
     *
     * @return string
     */
    public function getEscCurDesc()
    {
        return $this->escCurDesc;
    }

    /**
     * Set escCurInteres
     *
     * @param integer $escCurInteres
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurInteres($escCurInteres)
    {
        $this->escCurInteres = $escCurInteres;

        return $this;
    }

    /**
     * Get escCurInteres
     *
     * @return integer
     */
    public function getEscCurInteres()
    {
        return $this->escCurInteres;
    }

    /**
     * Set escCurInteresDet
     *
     * @param integer $escCurInteresDet
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurInteresDet($escCurInteresDet)
    {
        $this->escCurInteresDet = $escCurInteresDet;

        return $this;
    }

    /**
     * Get escCurInteresDet
     *
     * @return integer
     */
    public function getEscCurInteresDet()
    {
        return $this->escCurInteresDet;
    }

    /**
     * Set idCiudadPrin
     *
     * @param integer $idCiudadPrin
     *
     * @return RegEscuelasCursos
     */
    public function setIdCiudadPrin($idCiudadPrin)
    {
        $this->idCiudadPrin = $idCiudadPrin;

        return $this;
    }

    /**
     * Get idCiudadPrin
     *
     * @return integer
     */
    public function getIdCiudadPrin()
    {
        return $this->idCiudadPrin;
    }

    /**
     * Set escCurPromo1
     *
     * @param string $escCurPromo1
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurPromo1($escCurPromo1)
    {
        $this->escCurPromo1 = $escCurPromo1;

        return $this;
    }

    /**
     * Get escCurPromo1
     *
     * @return string
     */
    public function getEscCurPromo1()
    {
        return $this->escCurPromo1;
    }

    /**
     * Set escCurPromo2
     *
     * @param string $escCurPromo2
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurPromo2($escCurPromo2)
    {
        $this->escCurPromo2 = $escCurPromo2;

        return $this;
    }

    /**
     * Get escCurPromo2
     *
     * @return string
     */
    public function getEscCurPromo2()
    {
        return $this->escCurPromo2;
    }

    /**
     * Set escCurCampus
     *
     * @param string $escCurCampus
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurCampus($escCurCampus)
    {
        $this->escCurCampus = $escCurCampus;

        return $this;
    }

    /**
     * Get escCurCampus
     *
     * @return string
     */
    public function getEscCurCampus()
    {
        return $this->escCurCampus;
    }

    /**
     * Set escCurNivelSe
     *
     * @param boolean $escCurNivelSe
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurNivelSe($escCurNivelSe)
    {
        $this->escCurNivelSe = $escCurNivelSe;

        return $this;
    }

    /**
     * Get escCurNivelSe
     *
     * @return boolean
     */
    public function getEscCurNivelSe()
    {
        return $this->escCurNivelSe;
    }

    /**
     * Set escCurGrupos
     *
     * @param boolean $escCurGrupos
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurGrupos($escCurGrupos)
    {
        $this->escCurGrupos = $escCurGrupos;

        return $this;
    }

    /**
     * Get escCurGrupos
     *
     * @return boolean
     */
    public function getEscCurGrupos()
    {
        return $this->escCurGrupos;
    }

    /**
     * Set escCurRequisitos
     *
     * @param string $escCurRequisitos
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurRequisitos($escCurRequisitos)
    {
        $this->escCurRequisitos = $escCurRequisitos;

        return $this;
    }

    /**
     * Get escCurRequisitos
     *
     * @return string
     */
    public function getEscCurRequisitos()
    {
        return $this->escCurRequisitos;
    }

    /**
     * Set escCurMaterias
     *
     * @param string $escCurMaterias
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurMaterias($escCurMaterias)
    {
        $this->escCurMaterias = $escCurMaterias;

        return $this;
    }

    /**
     * Get escCurMaterias
     *
     * @return string
     */
    public function getEscCurMaterias()
    {
        return $this->escCurMaterias;
    }

    /**
     * Set escCurAreasTrabajo
     *
     * @param string $escCurAreasTrabajo
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurAreasTrabajo($escCurAreasTrabajo)
    {
        $this->escCurAreasTrabajo = $escCurAreasTrabajo;

        return $this;
    }

    /**
     * Get escCurAreasTrabajo
     *
     * @return string
     */
    public function getEscCurAreasTrabajo()
    {
        return $this->escCurAreasTrabajo;
    }

    /**
     * Set escCurEstatus
     *
     * @param boolean $escCurEstatus
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurEstatus($escCurEstatus)
    {
        $this->escCurEstatus = $escCurEstatus;

        return $this;
    }

    /**
     * Get escCurEstatus
     *
     * @return boolean
     */
    public function getEscCurEstatus()
    {
        return $this->escCurEstatus;
    }

    /**
     * Set escCurInmersionIdi
     *
     * @param boolean $escCurInmersionIdi
     *
     * @return RegEscuelasCursos
     */
    public function setEscCurInmersionIdi($escCurInmersionIdi)
    {
        $this->escCurInmersionIdi = $escCurInmersionIdi;

        return $this;
    }

    /**
     * Get escCurInmersionIdi
     *
     * @return boolean
     */
    public function getEscCurInmersionIdi()
    {
        return $this->escCurInmersionIdi;
    }

    /**
     * Set idCarga
     *
     * @param integer $idCarga
     *
     * @return RegEscuelasCursos
     */
    public function setIdCarga($idCarga)
    {
        $this->idCarga = $idCarga;

        return $this;
    }

    /**
     * Get idCarga
     *
     * @return integer
     */
    public function getIdCarga()
    {
        return $this->idCarga;
    }

    /**
     * Set escFicha
     *
     * @param string $escFicha
     *
     * @return RegEscuelasCursos
     */
    public function setEscFicha($escFicha)
    {
        $this->escFicha = $escFicha;

        return $this;
    }

    /**
     * Get escFicha
     *
     * @return string
     */
    public function getEscFicha()
    {
        return $this->escFicha;
    }

    /**
     * Set escFichaNpag
     *
     * @param boolean $escFichaNpag
     *
     * @return RegEscuelasCursos
     */
    public function setEscFichaNpag($escFichaNpag)
    {
        $this->escFichaNpag = $escFichaNpag;

        return $this;
    }

    /**
     * Get escFichaNpag
     *
     * @return boolean
     */
    public function getEscFichaNpag()
    {
        return $this->escFichaNpag;
    }

    /**
     * Set escVideos
     *
     * @param boolean $escVideos
     *
     * @return RegEscuelasCursos
     */
    public function setEscVideos($escVideos)
    {
        $this->escVideos = $escVideos;

        return $this;
    }

    /**
     * Get escVideos
     *
     * @return boolean
     */
    public function getEscVideos()
    {
        return $this->escVideos;
    }

    /**
     * Set idCurso
     *
     * @param \AdminBundle\Entity\CatCursos $idCurso
     *
     * @return RegEscuelasCursos
     */
    public function setIdCurso(\AdminBundle\Entity\CatCursos $idCurso = null)
    {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \AdminBundle\Entity\CatCursos
     */
    public function getIdCurso()
    {
        return $this->idCurso;
    }

    /**
     * Set idEscuela
     *
     * @param \AdminBundle\Entity\CatEscuelas $idEscuela
     *
     * @return RegEscuelasCursos
     */
    public function setIdEscuela(\AdminBundle\Entity\CatEscuelas $idEscuela = null)
    {
        $this->idEscuela = $idEscuela;

        return $this;
    }

    /**
     * Get idEscuela
     *
     * @return \AdminBundle\Entity\CatEscuelas
     */
    public function getIdEscuela()
    {
        return $this->idEscuela;
    }
}

