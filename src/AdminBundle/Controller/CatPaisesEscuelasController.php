<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\CatPaisesEscuelas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AdminBundle\Entity\Image;

/**
 * Catpaisesescuela controller.
 *
 */
class CatPaisesEscuelasController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all catPaisesEscuela entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catPaisesEscuelas = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();

        return $this->render('AdminBundle:CatPaisesEscuelas:index.html.twig', array(
            'catPaisesEscuelas' => $catPaisesEscuelas,
        ));
    }

    /**
     * Creates a new catPaisesEscuela entity.
     *
     */
    public function newAction(Request $request)
    {
        $catPaisesEscuela = new CatPaisesEscuelas();
        $form = $this->createForm('AdminBundle\Form\CatPaisesEscuelasType', $catPaisesEscuela);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //file flag
            $file_post = $form->get("pa07")->getData();
            //file modal
            $file_modal = $form->get("imgModal")->getData();
            $title = $form->get("paisDesc")->getData();

            if (!empty($file_post) && $file_post != null && !empty($file_modal) && $file_modal != null) {

                $ext = $file_post->guessExtension();
                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/banderas/paises/";
                    $file_post->move($path_of_file, $file_name);

                    //insert table image
                    $image_flag = new Image();
                    $image_flag->setName($file_name);
                    $image_flag->setPath("uploads/banderas/paises/" . $file_name);
                    $image_flag->setCreatedAt(new \DateTime());
                    $image_flag->setType("banderas");
                    $em->persist($image_flag);
                    $em->flush();

                    /*********/

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/popups/paises/";
                    $file_modal->move($path_of_file, $file_name);

                    //insert table image
                    $image_modal = new Image();
                    $image_modal->setName($file_name);
                    $image_modal->setPath("uploads/popups/paises/" . $file_name);
                    $image_modal->setCreatedAt(new \DateTime());
                    $image_modal->setType("popups");
                    $em->persist($image_modal);
                    $em->flush();

                    //Last pais in DB
                    $repository = $em->getRepository('AdminBundle:CatPaisesEscuelas');
                    $ultimo = $repository->createQueryBuilder('t')
                        ->where('t.idPais IS NOT NULL')
                        ->orderBy('t.idPais', 'DESC')
                        ->getQuery()->setMaxResults(1)->getOneOrNullResult();

                    if ($ultimo != null) {
                        $data = $ultimo->getIdPais();
                    } else {
                        $data = 0;
                    }

                    //Insert ids image
                    $catPaisesEscuela->setIdPais($data + 1);
                    $image_db = $em->getRepository('AdminBundle:Image')->find($image_flag->getId());
                    $catPaisesEscuela->setPa07($image_db);
                    $image_db = $em->getRepository('AdminBundle:Image')->find($image_modal->getId());
                    $catPaisesEscuela->setImgModal($image_db);
                    $em->persist($catPaisesEscuela);
                    $em->flush();

                    $msg = "Se creo correctamente el Pais";
                    $this->session->getFlashBag()->add("success", $msg);

                    return $this->redirectToRoute('catpaisesescuelas_index');
                } else {
                    $msg = "Formato invalido";
                    $this->session->getFlashBag()->add("danger", $msg);
                }
            } else {
                $catPaisesEscuela->setPa07(null);
                $catPaisesEscuela->setImgModal(null);
                $em->persist($catPaisesEscuela);
                $em->flush();
            }
        }

        return $this->render('AdminBundle:CatPaisesEscuelas:new.html.twig', array(
            'catPaisesEscuela' => $catPaisesEscuela,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a catPaisesEscuela entity.
     *
     */
    public function showAction(CatPaisesEscuelas $catPaisesEscuela)
    {
        $deleteForm = $this->createDeleteForm($catPaisesEscuela);

        return $this->render('catpaisesescuelas/show.html.twig', array(
            'catPaisesEscuela' => $catPaisesEscuela,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing catPaisesEscuela entity.
     *
     */
    public function editAction(Request $request, CatPaisesEscuelas $catPaisesEscuela)
    {
        $editForm = $this->createForm('AdminBundle\Form\CatPaisesEscuelasType', $catPaisesEscuela);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $id = $catPaisesEscuela->getIdPais();
        $content = $em->getRepository('AdminBundle:CatPaisesEscuelas')->find($id);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $file_post = $editForm->get("pa07")->getData();
            $file_modal = $editForm->get("imgModal")->getData();

            if ((!empty($file_post) && $file_post != null) || (!empty($file_modal) && $file_modal != null)) {

                $ext = '';
                $ext_2 = '';
                if ($file_post != null) {
                    $ext = $file_post->guessExtension();
                }
                if ($file_modal != null) {
                    $ext_2 = $file_modal->guessExtension();
                }

                if ($catPaisesEscuela->getPa07() == null || $catPaisesEscuela->getImgModal() == null) {
                    //If image not exist
                    if ($catPaisesEscuela->getPa07() == null && $file_post != null) {

                        $file_name_flag = time() . "." . $ext;
                        $path_of_file = "uploads/banderas/paises/";
                        $file_post->move($path_of_file, $file_name_flag);

                        //insert table image
                        $image_flag = new Image();
                        $image_flag->setName($file_name_flag);
                        $image_flag->setPath("uploads/banderas/paises/" . $file_name_flag);
                        $image_flag->setCreatedAt(new \DateTime());
                        $image_flag->setType("banderas");
                        $em->persist($image_flag);
                        $em->flush();

                        $catPaisesEscuela->setPa07($image_flag);
                        $em->persist($catPaisesEscuela);
                        $em->flush();

                    }

                    if ($catPaisesEscuela->getImgModal() == null && $file_modal != null) {

                        $file_name = time() . "." . $ext_2;
                        $path_of_file = "uploads/popups/paises/";
                        $file_modal->move($path_of_file, $file_name);

                        //insert table image
                        $image_modal = new Image();
                        $image_modal->setName($file_name);
                        $image_modal->setPath("uploads/popups/paises/" . $file_name);
                        $image_modal->setCreatedAt(new \DateTime());
                        $image_modal->setType("popups");
                        $em->persist($image_modal);
                        $em->flush();

                        $catPaisesEscuela->setImgModal($image_modal);
                        $em->persist($catPaisesEscuela);
                        $em->flush();
                    }

                    $msg = "Se edito correctamente el Pais";
                    $this->session->getFlashBag()->add("success", $msg);


                    return $this->redirectToRoute('catpaisesescuelas_index');
                } else {
                    //Update image if exist
                    if ($ext == 'jpeg' || $ext == "jpg" || $ext == "png" || $ext_2 == 'jpeg' || $ext_2 == "jpg" || $ext_2 == "png") {

                        if (!empty($file_post) && $file_post != null) {

                            $file_name_flag = time() . "." . $ext;
                            $path_of_file = "uploads/banderas/paises/";
                            $file_post->move($path_of_file, $file_name_flag);

                            //Update table image
                            $image_flag = $em->getRepository('AdminBundle:Image')->find($catPaisesEscuela->getPa07()->getId());
                            $image_flag->setName($file_name_flag);
                            $image_flag->setPath("uploads/banderas/paises/" . $file_name_flag);
                            $image_flag->setType("banderas");

                            $em->persist($image_flag);
                            $em->flush();

                        }

                        if (!empty($file_modal) && $file_post != null) {
                            $file_name_modal = time() . "." . $ext_2;
                            $path_of_file_2 = "uploads/popups/paises/";
                            $file_modal->move($path_of_file_2, $file_name_modal);

                            //Update table image
                            $image_modal = $em->getRepository('AdminBundle:Image')->find($catPaisesEscuela->getImgModal()->getId());
                            $image_modal->setName($file_name_modal);
                            $image_modal->setPath("uploads/popups/paises/" . $file_name_modal);
                            $image_flag->setType("popups");

                            $em->persist($image_modal);
                            $em->flush();

                        }

                        $msg = "Se edito correctamente el Pais";
                        $this->session->getFlashBag()->add("success", $msg);


                        return $this->redirectToRoute('catpaisesescuelas_index');
                    } else {
                        $msg = "Formato invalido para el archivo";
                        $this->session->getFlashBag()->add("danger", $msg);
                    }
                }


            } else {
                $this->getDoctrine()->getManager()->flush();

                $msg = "Se edito correctamente el pais";
                $this->session->getFlashBag()->add("success", $msg);

                return $this->redirectToRoute('catpaisesescuelas_index');
            }
        }

        return $this->render('AdminBundle:CatPaisesEscuelas:edit.html.twig', array(
            'catPaisesEscuela' => $catPaisesEscuela,
            'edit_form' => $editForm->createView(),
            'content' => $content
        ));
    }

    /**
     * Deletes a catPaisesEscuela entity.
     *
     */
    public function deleteAction(Request $request, $idPais = null)
    {

        $em = $this->getDoctrine()->getManager();
        $pais = $em->getRepository('AdminBundle:CatPaisesEscuelas')->find($idPais);

        $ciudades = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findBy(array(
            'idPais' => $pais->getIdPais()
        ));

        if (count($ciudades) > 0) {
            $status = '¡Cuidado!, no se puede borrar este pais, ya que tiene ciudades asignadas';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('catpaisesescuelas_index');
        } else {

            //Remove Image
            if ($pais->getPa07() != null) {
                $image = $em->getRepository('AdminBundle:Image')->find($pais->getPa07()->getId());
                $em->remove($image);
                $em->flush();
            }

            if ($pais->getImgModal() != null) {
                $image = $em->getRepository('AdminBundle:Image')->find($pais->getImgModal()->getId());
                $em->remove($image);
                $em->flush();
            }


            $em->remove($pais);
            $flush = $em->flush();

            if ($flush == null) {
                $status = 'El pais se ha borrado correctamente';
                $this->session->getFlashBag()->add("success", $status);

                return $this->redirectToRoute('catpaisesescuelas_index');
            } else {
                $status = 'El pais no se ha borrado correctamente';
                $this->session->getFlashBag()->add("danger", $status);

                return $this->redirectToRoute('catpaisesescuelas_index');
            }
        }


    }

    /**
     * Creates a form to delete a catPaisesEscuela entity.
     *
     * @param CatPaisesEscuelas $catPaisesEscuela The catPaisesEscuela entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createDeleteForm(CatPaisesEscuelas $catPaisesEscuela)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catpaisesescuelas_delete', array('idPais' => $catPaisesEscuela->getIdpais())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
