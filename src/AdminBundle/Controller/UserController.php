<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all user entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AdminBundle:User')->findAll();

        return $this->render('AdminBundle:User:index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new user entity.
     *
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AdminBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $query = $em->createQuery('SELECT u FROM AdminBundle:User u WHERE u.email = :email')
                ->setParameter('email', $form->get("email")->getData());
            $user_isset = $query->getResult();

            if (count($user_isset) == 0){
                $factory = $this->get("security.encoder_factory");
                $encoder = $factory->getEncoder($user);

                $password = $encoder->encodePassword($form->get("password")->getData(), $user->getSalt());

                $user->setPassword($password);
                //$user->setRole("ROLE_ADMIN");
                $em->persist($user);
                $flush = $em->flush();

                if ($flush == null) {
                    $msg = "Se creo correctamente el usuario";
                    $this->session->getFlashBag()->add("success", $msg);
                }

                return $this->redirectToRoute('users_index');
            } else {
                $msg = "Ya existe un usuario con ese email";
                $this->session->getFlashBag()->add("danger", $msg);
            }
        }

        return $this->render('AdminBundle:User:new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AdminBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $factory = $this->get("security.encoder_factory");
            $encoder = $factory->getEncoder($user);

            $password = $encoder->encodePassword($editForm->get("password")->getData(), $user->getSalt());
            $user->setPassword($password);

            $this->getDoctrine()->getManager()->flush();
            $msg = "Se ha editado correctamente el usuario";

            $this->session->getFlashBag()->add("success", $msg);
            return $this->redirectToRoute('users_index');
        }

        return $this->render('AdminBundle:User:edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     */
    public function deleteAction(Request $request, $id = null) {

        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:User')->find($id);

        $em->remove($content);
        $flush = $em->flush();

        if ($flush == null) {
            $status = 'El usuario se ha borrado correctamente';
            $this->session->getFlashBag()->add("success", $status);

            return $this->redirectToRoute('users_index');
        } else {
            $status = 'El usuarios no se ha borrado correctamente';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('users_index');
        }
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
