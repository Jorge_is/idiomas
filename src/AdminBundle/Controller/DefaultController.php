<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }

    public function loginAction()
    {
        if (is_object($this->getUser())) {
            if ($this->getUser()->getRole() == 'ROLE_ADMIN') {
                return $this->redirect('idiomas');
            }
        }

        $authenticationutils = $this->get("security.authentication_utils");

        $error = $authenticationutils->getLastAuthenticationError();
        $lastUsername = $authenticationutils->getLastUsername();

        return $this->render('AdminBundle:User:login.html.twig', array(
            "lastUsername" => $lastUsername,
            "error" => $error
        ));
    }
}
