<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Photogallery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Image;
use AdminBundle\Entity\Videos;
use AdminBundle\Entity\Block;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

/**
 * Photogallery controller.
 *
 */
class PhotogalleryController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all photogallery entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $photogalleries = $em->getRepository('AdminBundle:Photogallery')->findAll();

        return $this->render('AdminBundle:Photogallery:index.html.twig', array(
            'photogalleries' => $photogalleries,
        ));
    }

    /**
     * Creates a new photogallery entity.
     *
     */
    public function newAction(Request $request)
    {
        $photogallery = new Photogallery();
        $form = $this->createForm('AdminBundle\Form\PhotogalleryType', $photogallery);
        $form->handleRequest($request);

        //get school
        $escuela = $request->get('escuela');
        //get sede
        $sede = $request->get('sede');
        $pais = $request->get('pais');
        //get seccion
        $seccion = $form->get('seccion')->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //obtengo el pais
            $pais_db = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findOneBy(array(
                'paisDesc' => $pais
            ));

            //obtengo la ciudad
            $sede_db = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findOneBy(array(
                'ciudadDesc' => $sede,
                'idPais' => $pais_db->getIdPais()
            ));

            //obtengo la sede
            $scuela_db = $em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array(
                'escNombre' => $escuela,
                'idCiudad' => $sede_db->getIdCiudad()
            ));

            $photogallery_exist = $em->getRepository('AdminBundle:Photogallery')->findOneBy(array('escuela' => $scuela_db->getIdEscuela()));

            if (count($photogallery_exist) > 0) {
                $msg = "Error ya existe multimedia para la escuela seleccionada";
                $this->session->getFlashBag()->add("danger", $msg);

                return $this->redirect($this->generateUrl('photogallery_new'));
            } else {
                //get files actividad
                $files_actividad = $form->get("fileActividad")->getData();
                //get files galeria
                $files_galeria = $form->get("fileGaleria")->getData();
                //get asset video
                $files_video = $form->get("video")->getData();

                if ($files_actividad != null || !empty($files_actividad) || !empty($files_galeria) || $files_galeria != null || !empty($files_video) || $files_video != null) {
                    if (!empty($files_actividad) && $files_actividad != null) {
                        if ($files_actividad) {
                            foreach ($files_actividad as $file) {
                                if ($file != null) {
                                    //size control file, example 15.1 MB
                                    /*if (filesize($file) >= 15139845) {
                                        $pathInfo = $request->getPathInfo();
                                        $requestUri = $request->getRequestUri();

                                        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

                                        $msg = "Archivo(s) demasiado(s) pesado(s)";
                                        $this->session->getFlashBag()->add("danger", $msg);

                                        return $this->redirect($url, 301);
                                    } else {*/
                                    $ext = $file->guessExtension();
                                    if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                                        $file_name = md5(uniqid()) . "." . $ext;
                                        $path_of_file = "uploads/actividades/" . $escuela;
                                        $file->move($path_of_file, $file_name);

                                        //Insert table image
                                        $image = new Image();
                                        $image->setName($file_name);
                                        $image->setPath("uploads/actividades/" . $escuela . "/" . $file_name);
                                        $image->setCreatedAt(new \DateTime());
                                        $image->setType("actividades");

                                        $em->persist($image);
                                        $em->flush();

                                        //Inserto Id image
                                        $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
                                        if ($image_db != null) {
                                            $photogallery->addImage($image_db);
                                        }

                                        $em->persist($photogallery);
                                        $flush = $em->flush();

                                    } else {
                                        $msg = "Formato invalido";
                                        $this->session->getFlashBag()->add("danger", $msg);
                                    }
                                    /* }*/

                                }
                            }
                        }
                    }

                    if (!empty($files_galeria) && $files_galeria != null) {
                        if ($files_galeria) {
                            foreach ($files_galeria as $file) {
                                if ($file != null) {
                                    //size control file, example 15.1 MB
                                    /*if (filesize($file) >= 15139845) {
                                        $pathInfo = $request->getPathInfo();
                                        $requestUri = $request->getRequestUri();

                                        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

                                        $msg = "Archivo(s) demasiado(s) pesado(s)";
                                        $this->session->getFlashBag()->add("danger", $msg);

                                        return $this->redirect($url, 301);
                                    } else {*/
                                    $ext = $file->guessExtension();
                                    if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                                        $file_name = md5(uniqid()) . "." . $ext;
                                        $path_of_file = "uploads/photogallery/" . $escuela;
                                        $file->move($path_of_file, $file_name);

                                        //Insert table image
                                        $image = new Image();
                                        $image->setName($file_name);
                                        $image->setPath("uploads/photogallery/" . $escuela . "/" . $file_name);
                                        $image->setCreatedAt(new \DateTime());
                                        $image->setType("photogallery");

                                        $em->persist($image);
                                        $em->flush();

                                        //Inserto Id image
                                        $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
                                        if ($image_db != null) {
                                            $photogallery->addImage($image_db);
                                        }

                                        $em->persist($photogallery);
                                        $flush = $em->flush();

                                    } else {
                                        $msg = "Formato invalido";
                                        $this->session->getFlashBag()->add("danger", $msg);
                                    }
                                    /*}*/
                                }
                            }
                        }
                    }

                    if (!empty($files_video) && $files_video != null) {

                        if ($files_video) {
                            foreach ($files_video as $file) {
                                if ($file != null) {
                                    //Insert table video
                                    $video = new Videos();
                                    $video->setSrc($file);
                                    $video->setCreatedAt(new \DateTime());

                                    $em->persist($video);
                                    $em->flush();

                                    //Inserto Id video
                                    $video_db = $em->getRepository('AdminBundle:Videos')->find($video->getId());
                                    if ($video_db != null) {
                                        $photogallery->addVideo($video_db);
                                    }

                                    $em->persist($photogallery);
                                    $em->flush();
                                }
                            }
                        }
                    }

                    //Seteo escuela
                    $photogallery->setEscuela($scuela_db);
                    $em->persist($photogallery);
                    $em->flush();

                    //Creo block
                    $block = new Block();
                    $block->setEscuela($scuela_db);
                    $block->setSeccion($seccion);
                    $block->setNameBlockOne("Bloque Fotos y Videoss");
                    $em->persist($block);
                    $em->flush();

                    $msg = "Se creo correctamente la multimedia";
                    $this->session->getFlashBag()->add("success", $msg);

                    return $this->redirectToRoute('photogallery_index');
                }
            }

        }

        return $this->render('AdminBundle:Photogallery:new.html.twig', array(
            'photogallery' => $photogallery,
            'form' => $form->createView(),
        ));
    }

    /**
     * Delete image photogallery, via Ajax
     *
     */
    public function deleteImgAction(Request $request)
    {
        //$helpers = $this->get('app.helpers');
        $em = $this->getDoctrine()->getManager();

        //Variables AJAX
        $image_id = $request->request->get('image_id');
        $photogallery_id = $request->request->get('photogallery_id');

        $flag = false;

        //Obtengo el objeto de la photogalllery
        $photogallery = $em->getRepository('AdminBundle:Photogallery')->find($photogallery_id);
        //Obtengo las imagenes de la photogallery
        $photogallery_images = $photogallery->getImage();
        //Hago loop, recorriendo los images
        foreach ($photogallery_images as $image) {

            //Si el img id == al image_id de ajax
            if ($image->getId() == $image_id) {

                //Remove de tabla relacional
                $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
                $photogallery->removeImage($image_db);

                //Remove file
                $path_image = "uploads/" . $image_db->getType() . "/" . $photogallery->getEscuela()->getEscNombre() . "/" . $image_db->getName();

                if (file_exists($path_image)) {
                    unlink($path_image);
                } else {
                    //not found
                }

                $em->persist($photogallery);
                $flush = $em->flush();

                if ($flush == null) {
                    $flag = true;
                }
            }

        }

        if ($flag == true) {
            //Borro imagen de su Tabla
            $image_db = $em->getRepository('AdminBundle:Image')->find($image_id);
            $em->remove($image_db);
            $em->flush();

            $data = array(
                "status" => "success",
                "msg" => "Ejecucion exitosa"
            );

        } else {
            $data = array(
                "status" => "error",
                "msg" => "Ejecucion erronea"
            );
        }

        return new Response(json_encode($data));

    }

    /**
     * Delete video, via Ajax
     *
     */
    public
    function deleteVideoAction(Request $request)
    {
        //$helpers = $this->get('app.helpers');
        $em = $this->getDoctrine()->getManager();

        //Variables AJAX
        $video_id = $request->request->get('video_id');
        $photogallery_id = $request->request->get('photogallery_id');

        $flag = false;

        //Obtengo el objeto de la photogalllery
        $photogallery = $em->getRepository('AdminBundle:Photogallery')->find($photogallery_id);
        //Obtengo los videos de la photogallery
        $photogallery_videos = $photogallery->getVideo();
        //Hago loop, recorriendo los images
        foreach ($photogallery_videos as $image) {

            //Si el img id == al video_id de ajax
            if ($image->getId() == $video_id) {

                //Remove de tabla relacional
                $video_db = $em->getRepository('AdminBundle:Videos')->find($image->getId());
                $photogallery->removeVideo($video_db);

                $em->persist($photogallery);
                $flush = $em->flush();

                if ($flush == null) {
                    $flag = true;
                }
            }
        }

        if ($flag == true) {
            //Borro imagen de su Tabla
            $video_db = $em->getRepository('AdminBundle:Videos')->find($video_id);
            $em->remove($video_db);
            $em->flush();

            $data = array(
                "status" => "success",
                "msg" => "Ejecucion exitosa"
            );

        } else {
            $data = array(
                "status" => "error",
                "msg" => "Ejecucion erronea"
            );
        }

        return new Response(json_encode($data));

    }


    /**
     * Displays a form to edit an existing photogallery entity.
     *
     */
    public function editAction(Request $request, Photogallery $photogallery)
    {
        $editForm = $this->createForm('AdminBundle\Form\PhotogalleryType', $photogallery);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $escuela = $request->get('escuela');
        $sede = $request->get('sede');
        $pais = $request->get('pais');

        //obtengo el pais
        $pais_db = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findOneBy(array(
            'paisDesc' => $photogallery->getEscuela()->getIdPais()->getPaisDesc()
        ));


        //obtengo la ciudad
        $sede_db = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findOneBy(array(
            'idCiudad' => $photogallery->getEscuela()->getIdCiudad(),
            'idPais' => $pais_db->getIdPais()
        ));

        $name_sede = $sede_db->getCiudadDesc();
        $name_pais = $sede_db->getIdPais();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //get files actividad
            $files_actividad = $editForm->get("fileActividad")->getData();
            //get files galeria
            $files_galeria = $editForm->get("fileGaleria")->getData();
            //get asset video
            $files_video = $editForm->get("video")->getData();

            if ($files_actividad != null || !empty($files_actividad) || !empty($files_galeria) || $files_galeria != null || !empty($files_video) || $files_video != null) {
                if (!empty($files_actividad) && $files_actividad != null) {

                    if ($files_actividad) {
                        foreach ($files_actividad as $file) {
                            if ($file != null) {
                                //size control file, example 15.1 MB
                                /*if (filesize($file) >= 15139845) {
                                    $pathInfo = $request->getPathInfo();
                                    $requestUri = $request->getRequestUri();

                                    $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

                                    $msg = "Archivo(s) demasiado(s) pesado(s)";
                                    $this->session->getFlashBag()->add("danger", $msg);

                                    return $this->redirect($url, 301);
                                } else {*/
                                $ext = $file->guessExtension();
                                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                                    $file_name = md5(uniqid()) . "." . $ext;
                                    $path_of_file = "uploads/actividades/" . $escuela;
                                    $file->move($path_of_file, $file_name);

                                    //Insert table image
                                    $image = new Image();
                                    $image->setName($file_name);
                                    $image->setPath("uploads/actividades/" . $escuela . "/" . $file_name);
                                    $image->setCreatedAt(new \DateTime());
                                    $image->setType("actividades");

                                    $em->persist($image);
                                    $em->flush();

                                    //Inserto Id image
                                    $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
                                    if ($image_db != null) {
                                        $photogallery->addImage($image_db);
                                    }

                                    $em->persist($photogallery);
                                    $flush = $em->flush();

                                } else {
                                    $msg = "Formato invalido";
                                    $this->session->getFlashBag()->add("danger", $msg);
                                }
                                /*}*/

                            }

                        }

                    }

                }
                if (!empty($files_galeria) && $files_galeria != null) {
                    if ($files_galeria) {
                        foreach ($files_galeria as $item) {

                            if ($item != null) {
                                //size control file, example 15.1 MB
                                /*if (filesize($item) >= 15139845) {
                                    $pathInfo = $request->getPathInfo();
                                    $requestUri = $request->getRequestUri();

                                    $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

                                    $msg = "Archivo(s) demasiado(s) pesado(s)";
                                    $this->session->getFlashBag()->add("danger", $msg);

                                    return $this->redirect($url, 301);
                                } else {*/
                                $ext = $item->guessExtension();
                                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                                    $file_name = md5(uniqid()) . "." . $ext;
                                    $path_of_file = "uploads/photogallery/" . $escuela;
                                    $item->move($path_of_file, $file_name);

                                    //Insert table image
                                    $image = new Image();
                                    $image->setName($file_name);
                                    $image->setPath("uploads/photogallery/" . $escuela . "/" . $file_name);
                                    $image->setCreatedAt(new \DateTime());
                                    $image->setType("photogallery");

                                    $em->persist($image);
                                    $em->flush();

                                    //Inserto Id image
                                    $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
                                    if ($image_db != null) {
                                        $photogallery->addImage($image_db);
                                    }

                                    $em->persist($photogallery);
                                    $flush = $em->flush();

                                } else {
                                    $msg = "Formato invalido";
                                    $this->session->getFlashBag()->add("danger", $msg);
                                }
                                /*}*/

                            }

                        }

                    }

                }

                if (!empty($files_video) && $files_video != null) {
                    if ($files_video) {
                        foreach ($files_video as $item) {
                            if ($item != null) {
                                //Insert table video
                                $video = new Videos();
                                $video->setSrc($item);
                                $video->setCreatedAt(new \DateTime());

                                $em->persist($video);
                                $em->flush();

                                //Inserto Id video
                                $video_db = $em->getRepository('AdminBundle:Videos')->find($video->getId());
                                if ($video_db != null) {
                                    $photogallery->addVideo($video_db);
                                }

                                $em->persist($photogallery);
                                $em->flush();
                            }

                        }
                    }

                }

                //Seteo escuela
                $photogallery->setEscuela($em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array('escNombre' => $escuela, 'idCiudad' => $sede_db->getIdCiudad())));
                $em->persist($photogallery);
                $em->flush();

                $msg = "Se creo correctamente la multimedia";
                $this->session->getFlashBag()->add("success", $msg);

                return $this->redirectToRoute('photogallery_index');
            }

        }

        return $this->render('AdminBundle:Photogallery:edit.html.twig', array(
            'photogallery' => $photogallery,
            'edit_form' => $editForm->createView(),
            'sede' => $name_sede,
            'pais' => $name_pais
        ));
    }

    /**
     * Deletes a photogallery entity.
     *
     */
    public function deleteAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:Photogallery')->find($id);

        foreach ($content->getImage() as $image) {

            $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
            //delete tabla relacional
            $content->removeImage($image_db);
            //borro file fisico
            if (file_exists($image_db->getPath())) {
                unlink($image_db->getPath());
            } else {
                //not found
            }

            //remove imagen
            $em->remove($image_db);
            $em->flush();
        }

        //remove actividad
        $em->remove($content);

        $block = $em->getRepository('AdminBundle:Block')->findOneBy(array('escuela' => $content->getEscuela()->getIdEscuela()));
        //remove block
        if ($block != null) {
            $em->remove($block);
        }
        $flush = $em->flush();

        if ($flush == null) {
            $status = 'La foto gallery se ha borrado correctamente';
            $this->session->getFlashBag()->add("success", $status);

            return $this->redirectToRoute('photogallery_index');
        } else {
            $status = 'La foto gallery no se ha borrado correctamente';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('photogallery_index');
        }

    }


}
