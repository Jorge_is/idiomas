<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Actividad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Image;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Actividad controller.
 *
 */
class ActividadController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all Actividad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $actividads = $em->getRepository('AdminBundle:Actividad')->findAll();

        return $this->render('AdminBundle:Actividad:index.html.twig', array(
            'actividads' => $actividads,
        ));
    }

    /**
     * Creates a new Actividad entity.
     *
     */
    public function newAction(Request $request)
    {
        $actividad = new Actividad();
        $form = $this->createForm('AdminBundle\Form\ActividadType', $actividad);
        $form->handleRequest($request);

        $escuela = $request->get('escuela');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $file_post = $form->get("file")->getData();

            if (!empty($file_post) && $file_post != null) {

                $ext = $file_post->guessExtension();
                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/actividades/" . $escuela;
                    $file_post->move($path_of_file, $file_name);

                    //Insert table image
                    $image = new Image();
                    $image->setName($file_name);
                    $image->setPath("uploads/actividades/" . $escuela . "/" . $file_name);
                    $image->setCreatedAt(new \DateTime());

                    $em->persist($image);
                    $em->flush();

                    //Inserto Id image
                    $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
                    $actividad->setImage($image_db);

                    //Seteo escuela
                    $actividad->setEscuela($em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array('escNombre' => $escuela)));
                    $em->persist($actividad);
                    $flush = $em->flush();

                    if ($flush == null) {
                        $msg = "Se creo correctamente la actividad";
                        $this->session->getFlashBag()->add("success", $msg);
                    }

                    return $this->redirectToRoute('actividades_index');
                } else {
                    $msg = "Formato invalido";
                    $this->session->getFlashBag()->add("danger", $msg);
                }
            } else {
                $actividad->setImage(null);
                $em->persist($actividad);
                $em->flush();
            }
        }

        return $this->render('AdminBundle:Actividad:new.html.twig', array(
            'Actividad' => $actividad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Actividad entity.
     *
     */
    public function editAction(Request $request, Actividad $actividad)
    {
        $editForm = $this->createForm('AdminBundle\Form\ActividadType', $actividad);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $escuela = $request->get('escuela');

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $file_post = $editForm->get("file")->getData();

            if (!empty($file_post) && $file_post != null) {

                $ext = $file_post->guessExtension();
                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/actividades/" . $escuela;
                    $file_post->move($path_of_file, $file_name);

                    //Update table image
                    $image = $em->getRepository('AdminBundle:Image')->find($actividad->getImage()->getId());
                    $image->setName($file_name);
                    $image->setPath("uploads/actividades/" . $escuela . "/" . $file_name);

                    $em->persist($image);
                    $em->flush();

                    //update escuela
                    $actividad_db = $em->getRepository('AdminBundle:Actividad')->find($actividad->getId());
                    $actividad_db->setEscuela($em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array('escNombre' => $escuela)));
                    $em->persist($actividad_db);

                    $msg = "Se edito correctamente la actividad";
                    $this->session->getFlashBag()->add("success", $msg);

                    return $this->redirectToRoute('actividades_index');
                } else {
                    $msg = "Formato invalido para el archivo";
                    $this->session->getFlashBag()->add("danger", $msg);
                }
            } else {
                $this->getDoctrine()->getManager()->flush();

                $msg = "Se edito correctamente la actividad";
                $this->session->getFlashBag()->add("success", $msg);

                return $this->redirectToRoute('actividades_index');
            }

        }

        return $this->render('AdminBundle:Actividad:edit.html.twig', array(
            'actividad' => $actividad,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a Actividad entity.
     *
     */
    public function deleteAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:Actividad')->find($id);

        $image_db = $em->getRepository('AdminBundle:Image')->find($content->getImage()->getId());

        //borro file fisico
        if (file_exists($image_db->getPath())) {
            unlink($image_db->getPath());
        } else {
            //not found
        }

        //remove actividad
        $em->remove($content);
        $flush = $em->flush();

        //remove imagen
        $em->remove($image_db);
        $em->flush();

        if ($flush == null) {
            $status = 'La actividad se ha borrado correctamente';
            $this->session->getFlashBag()->add("success", $status);

            return $this->redirectToRoute('actividades_index');
        } else {
            $status = 'La actividad no se ha borrado correctamente';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('actividades_index');
        }

        return $this->redirectToRoute('actividades_index');
    }

    /**
     * Creates a form to delete a Actividad entity.
     *
     * @param Actividad $actividad The Actividad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Actividad $actividad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('actividades_delete', array('id' => $actividad->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
