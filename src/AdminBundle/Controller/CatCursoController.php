<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\CatCursos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AdminBundle\Entity\RegEscuelasCursos;
use Symfony\Component\HttpFoundation\Response;

/**
 * Catcurso controller.
 *
 */
class CatCursoController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all catCurso entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catCursos = $em->getRepository('AdminBundle:CatCursos')->findAll();

        return $this->render('AdminBundle:Catcurso:index.html.twig', array(
            'catCursos' => $catCursos,
        ));
    }

    /**
     * Creates a new catCurso entity.
     *
     */
    public function newAction(Request $request)
    {
        $catCurso = new CatCursos();
        $form = $this->createForm('AdminBundle\Form\CatCursoType', $catCurso);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $paises = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            //Escuela Id
            $escuela = $request->get('escuela');

            //$catCurso->setEscuela($em->getRepository('AdminBundle:CatEscuelas')->find($escuela));
            //Last ciudad in DB
            $repository = $em->getRepository('AdminBundle:CatCursos');
            $ultimo = $repository->createQueryBuilder('t')
                ->where('t.idCurso IS NOT NULL')
                ->orderBy('t.idCurso', 'DESC')
                ->getQuery()->setMaxResults(1)->getOneOrNullResult();

            if ($ultimo != null) {
                $data = $ultimo->getIdCurso();
            } else {
                $data = 0;
            }

            $catCurso->setIdCurso($data + 1);
            $catCurso->setIdPrograma(3);

            $em->persist($catCurso);
            $flush = $em->flush();

            //Insercion en la otra BD
            $reg_esc_cursos = new RegEscuelasCursos();
            $reg_esc_cursos->setIdEscuela($em->getRepository('AdminBundle:CatEscuelas')->find($escuela));
            $reg_esc_cursos->setIdCurso($em->getRepository('AdminBundle:CatCursos')->find($catCurso->getIdCurso()));
            $reg_esc_cursos->setIdIdioma($form->get("idioma")->getData()->getId());
            $reg_esc_cursos->setIdCursoD(1);
            $reg_esc_cursos->setEscCurNombre($form->get("curNombre")->getData());
            $reg_esc_cursos->setEscCurInteres(null);
            $reg_esc_cursos->setEscCurInteresDet(null);

            $em->persist($reg_esc_cursos);
            $flush = $em->flush();

            if ($flush == null) {
                $msg = "Se creo correctamente el Curso";
                $this->session->getFlashBag()->add("success", $msg);
            }

            return $this->redirectToRoute('catcursos_index');
        }

        return $this->render('AdminBundle:Catcurso:new.html.twig', array(
            'catCurso' => $catCurso,
            'form' => $form->createView(),
            'paises' => $paises
        ));
    }

    /**
     * Finds and displays a catCurso entity.
     *
     */
    public function showAction(CatCurso $catCurso)
    {
        $deleteForm = $this->createDeleteForm($catCurso);

        return $this->render('catcurso/show.html.twig', array(
            'catCurso' => $catCurso,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing catCurso entity.
     *
     */
    public function editAction(Request $request, CatCursos $catCurso)
    {
        $editForm = $this->createForm('AdminBundle\Form\CatCursoType', $catCurso);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $id = $catCurso->getIdCurso();

        $paises = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em->persist($catCurso);
            $em->flush();

            //Escuela Id
            $escuela = $request->get('escuela');

            //Update en la otra BD
            /*$reg_esc_cursos = $em->getRepository('AdminBundle:RegEscuelasCursos')->findOneBy(
                array('idCurso' => $catCurso->getIdCurso())
            );

            if (count($reg_esc_cursos) == 1) {
                $escuela_db = $em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array('idEscuela' => $escuela));
                $reg_esc_cursos->setIdEscuela($escuela_db);
                $reg_esc_cursos->setIdCurso($em->getRepository('AdminBundle:CatCursos')->find($catCurso->getIdCurso()));
                $reg_esc_cursos->setIdIdioma($editForm->get("idioma")->getData()->getId());
                $reg_esc_cursos->setEscCurNombre($editForm->get("curNombre")->getData());

                $em->persist($reg_esc_cursos);
                $flush = $em->flush();

                if ($flush == null) {
                    $msg = "Se edito correctamente el Curso";
                    $this->session->getFlashBag()->add("success", $msg);
                }

                return $this->redirectToRoute('catcursos_index');
            }*/

            $msg = "Se edito correctamente el Curso";
            $this->session->getFlashBag()->add("success", $msg);

            return $this->redirectToRoute('catcursos_index');

        }

        $content = $em->getRepository('AdminBundle:RegEscuelasCursos')->findOneBy(array(
            'idCurso' => $id
        ));

        return $this->render('AdminBundle:Catcurso:edit.html.twig', array(
            'catCurso' => $catCurso,
            'edit_form' => $editForm->createView(),
            'content' => $content,
            'paises' => $paises
        ));
    }

    /**
     * Deletes a catCurso entity.
     *
     */
    public function deleteAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:CatCursos')->find($id);

        $curso_content = $em->getRepository('AdminBundle:CursoContent')->findBy(array(
            'curso' => $content->getIdCurso()
        ));

        if (count($curso_content) > 0) {
            $status = '¡Cuidado! El curso no se ha borrado correctamente, ya que tiene contenido registrado';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('catcursos_index');
        } else {

            //delete registro reg_escuelas_cursos
            $reg_esc_cur = $em->getRepository('AdminBundle:RegEscuelasCursos')->findOneBy(array(
                'idCurso' => $id
            ));

            $em->remove($reg_esc_cur);

            $em->remove($content);
            $flush = $em->flush();

            if ($flush == null) {
                $status = 'El curso se ha borrado correctamente';
                $this->session->getFlashBag()->add("success", $status);

                return $this->redirectToRoute('catcursos_index');
            } else {
                $status = 'El curso no se ha borrado correctamente';
                $this->session->getFlashBag()->add("danger", $status);

                return $this->redirectToRoute('catcursos_index');
            }
        }


    }

    /**
     * Creates a form to delete a catCurso entity.
     *
     * @param CatCurso $catCurso The catCurso entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CatCurso $catCurso)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catcursos_delete', array('id' => $catCurso->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /*
     * Ajax ciudades
     */
    public function ciudadesAction(Request $request)
    {
        $pais_id = $request->request->get('pais_id');
        $em = $this->getDoctrine()->getManager();
        $ciudades = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findBy(array(
            "idPais" => $pais_id
        ));

        $ciudades_array = array();
        foreach ($ciudades as $ciudad) {
            $ciudades_array[] = array(
                'id' => $ciudad->getIdCiudad(),
                'name' => $ciudad->getCiudadDesc()
            );
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($ciudades_array));

        return $response;
    }

    /*
     * Ajax escuelas
     */
    public function escuelasAction(Request $request)
    {
        $ciudad_id = $request->request->get('ciudad_id');
        $em = $this->getDoctrine()->getManager();
        $escuelas = $em->getRepository('AdminBundle:CatEscuelas')->findBy(array(
            "idCiudad" => $ciudad_id
        ));

        $escuelas_array = array();
        foreach ($escuelas as $escuela) {
            $escuelas_array[] = array(
                'id' => $escuela->getIdEscuela(),
                'name' => $escuela->getEscnombre()
            );
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($escuelas_array));

        return $response;
    }

    /*
     * Ajax cursos
     */
    public function cursosAction(Request $request)
    {
        $escuela_id = $request->request->get('escuela_id');
        $em = $this->getDoctrine()->getManager();
        $cursos = $em->getRepository('AdminBundle:CatCurso')->findBy(array(
            "escuela" => $escuela_id
        ));

        $cursos_array = array();
        foreach ($cursos as $curso) {
            $cursos_array[] = array(
                'id' => $curso->getId(),
                'name' => $curso->getName()
            );
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($cursos_array));

        return $response;
    }


}
