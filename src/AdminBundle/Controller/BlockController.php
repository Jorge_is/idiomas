<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Block;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Block controller.
 *
 */
class BlockController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all block entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $blocks = $em->getRepository('AdminBundle:Block')->findAll();

        return $this->render('AdminBundle:Block:index.html.twig', array(
            'blocks' => $blocks,
        ));
    }

    /**
     * Creates a new block entity.
     *
     */
    public function newAction(Request $request)
    {
        $block = new Block();
        $form = $this->createForm('AdminBundle\Form\BlockType', $block);
        $form->handleRequest($request);

        //Name escuela
        $escuela = $request->get('escuela');
        $sede = $request->get('sede');
        $pais = $request->get('pais');

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            //obtengo el pais
            $pais_db = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findOneBy(array(
                'paisDesc' => $pais
            ));
            //obtengo la ciudad
            $sede_db = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findOneBy(array(
                'ciudadDesc' => $sede,
                'idPais' => $pais_db->getIdPais()
            ));

            //obtengo la escuela por nombre y ciudad
            $escuela_db = $em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array(
                'escNombre' => $escuela,
                'idCiudad' => $sede_db->getIdCiudad()
            ));

            if (!$escuela_db) {
                $status = 'No existe registro de la escuela seleccionada';

                $this->session->getFlashBag()->add("danger", $status);
                return $this->redirect($this->generateUrl('blocks_new'));
            } else {
                $escuela_exist = $em->getRepository('AdminBundle:Block')->findOneBy(array(
                    'escuela' => $escuela_db->getIdEscuela(),
                    'seccion' => $form->get("seccion")->getData()
                ));

                if (count($escuela_exist) > 0) {
                    $status = 'Ya existe una Ficha para esta escuela';
                    $this->session->getFlashBag()->add("danger", $status);
                    return $this->redirect($this->generateUrl('blocks_new'));

                } else {
                    $block->setEscuela($em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array('escNombre' => $escuela, 'idCiudad' => $sede_db->getIdCiudad())));
                    $em->persist($block);
                    $em->flush();

                    $status = 'Se creo correctamente el registro';
                    $this->session->getFlashBag()->add("success", $status);
                    return $this->redirectToRoute('blocks_index');
                }
            }
        }

        return $this->render('AdminBundle:Block:new.html.twig', array(
            'block' => $block,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a block entity.
     *
     */
    public function showAction(Block $block)
    {
        $deleteForm = $this->createDeleteForm($block);

        return $this->render('block/show.html.twig', array(
            'block' => $block,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing block entity.
     *
     */
    public function editAction(Request $request, Block $block)
    {
        $editForm = $this->createForm('AdminBundle\Form\BlockType', $block);
        $editForm->handleRequest($request);

        //Name escuela
        $escuela = $request->get('escuela');
        $sede = $request->get('sede');
        $pais = $request->get('pais');

        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //obtengo el pais
            $pais_db = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findOneBy(array(
                'paisDesc' => $pais
            ));
            //obtengo la ciudad
            $sede_db = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findOneBy(array(
                'ciudadDesc' => $sede,
                'idPais' => $pais_db->getIdPais()
            ));

            $em = $this->getDoctrine()->getManager();
            $block->setEscuela($em->getRepository('AdminBundle:CatEscuelas')->findOneBy(
                array(
                    'escNombre' => $escuela,
                    'idCiudad' => $sede_db->getIdCiudad()
                )));
            $em->persist($block);
            $em->flush();

            return $this->redirectToRoute('blocks_index');
        }

        return $this->render('AdminBundle:Block:edit.html.twig', array(
            'block' => $block,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a block entity.
     *
     */
    public function deleteAction(Request $request, $id = null)
    {

        $em = $this->getDoctrine()->getManager();
        $block = $em->getRepository('AdminBundle:Block')->find($id);

        $em->remove($block);
        $flush = $em->flush();

        if ($flush == null) {
            $status = 'El bloque se ha borrado correctamente';
            $this->session->getFlashBag()->add("success", $status);

            return $this->redirectToRoute('blocks_index');
        } else {
            $status = 'El bloque no se ha borrado correctamente';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('blocks_index');
        }
    }

    /**
     * Creates a form to delete a block entity.
     *
     * @param Block $block The block entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Block $block)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blocks_delete', array('id' => $block->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
