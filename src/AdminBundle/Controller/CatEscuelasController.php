<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\CatEscuelas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
Use AdminBundle\Entity\Image;

/**
 * Catescuela controller.
 *
 */
class CatEscuelasController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all catEscuela entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catEscuelas = $em->getRepository('AdminBundle:CatEscuelas')->findAll();

        return $this->render('AdminBundle:CatEscuelas:index.html.twig', array(
            'catEscuelas' => $catEscuelas,
        ));
    }

    /**
     * Creates a new catEscuela entity.
     *
     */
    public function newAction(Request $request)
    {
        $catEscuela = new CatEscuelas();
        $form = $this->createForm('AdminBundle\Form\CatEscuelasType', $catEscuela);
        $form->handleRequest($request);

        //Escuela Id
        $pais = $request->get('pais');
        $ciudad = $request->get('ciudad');

        $em = $this->getDoctrine()->getManager();

        $paises = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();

        if ($form->isSubmitted() && $form->isValid()) {

            $escuela_exist = $em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array(
                'escNombre' => $form->get("escNombre")->getData(),
                'idPais' => $pais,
                'idCiudad' => $ciudad
            ));

            if (count($escuela_exist) > 0) {
                $msg = "Ya existe una escuela con los mismos datos";
                $this->session->getFlashBag()->add("danger", $msg);

            } else {
                //file modal
                $file_modal = $form->get("esc19")->getData();
                $file_banner = $form->get("banner")->getData();

                if (!empty($file_modal) && $file_modal != null && !empty($file_banner) && $file_banner != null) {

                    $ext = $file_modal->guessExtension();
                    if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                        $file_name = time() . "." . $ext;
                        $path_of_file = "uploads/banners/escuelas/";
                        $file_banner->move($path_of_file, $file_name);

                        //insert table image
                        $image_flag = new Image();
                        $image_flag->setName($file_name);
                        $image_flag->setPath("uploads/banners/escuelas/" . $file_name);
                        $image_flag->setCreatedAt(new \DateTime());
                        $image_flag->setType("banners");
                        $em->persist($image_flag);
                        $em->flush();

                        /*********/

                        $file_name = time() . "." . $ext;
                        $path_of_file = "uploads/logos/escuelas/";
                        $file_modal->move($path_of_file, $file_name);

                        //insert table image
                        $image_modal = new Image();
                        $image_modal->setName($file_name);
                        $image_modal->setPath("uploads/logos/escuelas/" . $file_name);
                        $image_modal->setCreatedAt(new \DateTime());
                        $image_modal->setType("logos");
                        $em->persist($image_modal);
                        $em->flush();

                        //Last escuela in DB
                        $repository = $em->getRepository('AdminBundle:CatEscuelas');
                        $ultimo = $repository->createQueryBuilder('t')
                            ->where('t.idEscuela IS NOT NULL')
                            ->orderBy('t.idEscuela', 'DESC')
                            ->getQuery()->setMaxResults(1)->getOneOrNullResult();

                        if ($ultimo != null) {
                            $data = $ultimo->getIdEscuela();
                        } else {
                            $data = 0;
                        }

                        $catEscuela->setIdEscuela($data + 1);
                        $catEscuela->setIdMoneda(1);

                        $catEscuela->setIdPais($em->getRepository('AdminBundle:CatPaisesEscuelas')->find($pais));
                        $catEscuela->setIdCiudad($em->getRepository('AdminBundle:CatCiudadesEscuelas')->findOneBy(array('idCiudad' => $ciudad)));


                        //Insert ids image
                        $image_db = $em->getRepository('AdminBundle:Image')->find($image_flag->getId());
                        $catEscuela->setBanner($image_db);
                        $image_db = $em->getRepository('AdminBundle:Image')->find($image_modal->getId());
                        $catEscuela->setEsc19($image_db);
                        $em->persist($catEscuela);
                        $em->flush();

                        $msg = "Se creo correctamente la escuela";
                        $this->session->getFlashBag()->add("success", $msg);

                        return $this->redirectToRoute('catescuelas_index');
                    } else {
                        $msg = "Formato invalido";
                        $this->session->getFlashBag()->add("danger", $msg);
                    }
                } else {
                    $catEscuela->setEsc19(null);
                    $catEscuela->setBanner(null);
                    $em->persist($catEscuela);
                    $em->flush();
                }
            }
        }

        return $this->render('AdminBundle:CatEscuelas:new.html.twig', array(
            'catEscuela' => $catEscuela,
            'form' => $form->createView(),
            'paises' => $paises
        ));
    }

    /**
     * Finds and displays a catEscuela entity.
     *
     */
    public function showAction(CatEscuelas $catEscuela)
    {
        $deleteForm = $this->createDeleteForm($catEscuela);

        return $this->render('catescuelas/show.html.twig', array(
            'catEscuela' => $catEscuela,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing catEscuela entity.
     *
     */
    public function editAction(Request $request, CatEscuelas $catEscuela)
    {
        $editForm = $this->createForm('AdminBundle\Form\CatEscuelasType', $catEscuela);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $id = $catEscuela->getIdEscuela();
        $content = $em->getRepository('AdminBundle:CatEscuelas')->find($id);
        $paises = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();

        //Escuela Id
        $pais = $request->get('pais');
        $ciudad = $request->get('ciudad');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file_modal = $editForm->get("esc19")->getData();
            $file_banner = $editForm->get("banner")->getData();

            if ((!empty($file_banner) && $file_banner != null) || (!empty($file_modal) && $file_modal != null)) {

                $ext = '';
                $ext_2 = '';
                if ($file_banner != null) {
                    $ext = $file_banner->guessExtension();
                }
                if ($file_modal != null) {
                    $ext_2 = $file_modal->guessExtension();
                }

                if ($catEscuela->getEsc19() == null || $catEscuela->getBanner() == null) {
                    //If image not exist
                    if ($catEscuela->getBanner() == null && $file_banner != null) {

                        $file_name_flag = time() . "." . $ext;
                        $path_of_file = "uploads/banners/escuelas/";
                        $file_banner->move($path_of_file, $file_name_flag);

                        //insert table image
                        $image_flag = new Image();
                        $image_flag->setName($file_name_flag);
                        $image_flag->setPath("uploads/banners/escuelas/" . $file_name_flag);
                        $image_flag->setCreatedAt(new \DateTime());
                        $image_flag->setType("banners");
                        $em->persist($image_flag);
                        $em->flush();

                        $catEscuela->setBanner($image_flag);
                        $em->persist($catEscuela);
                        $em->flush();

                    }
                    if ($catEscuela->getEsc19() == null && $file_modal != null) {

                        $file_name = time() . "." . $ext_2;
                        $path_of_file = "uploads/logos/escuelas/";
                        $file_modal->move($path_of_file, $file_name);

                        //insert table image
                        $image_modal = new Image();
                        $image_modal->setName($file_name);
                        $image_modal->setPath("uploads/logos/escuelas/" . $file_name);
                        $image_modal->setCreatedAt(new \DateTime());
                        $image_modal->setType("logos");
                        $em->persist($image_modal);
                        $em->flush();

                        $catEscuela->setEsc19($image_modal);
                        $em->persist($catEscuela);
                        $em->flush();
                    }

                    $catEscuela->setIdPais($em->getRepository('AdminBundle:CatPaisesEscuelas')->find($pais));
                    $catEscuela->setIdCiudad($em->getRepository('AdminBundle:CatCiudadesEscuelas')->findOneBy(array('idCiudad' => $ciudad)));

                    $em->persist($catEscuela);
                    $em->flush();

                    $msg = "Se edito correctamente la escuela";
                    $this->session->getFlashBag()->add("success", $msg);

                    return $this->redirectToRoute('catescuelas_index');
                } else {

                    if ($ext == 'jpeg' || $ext == "jpg" || $ext == "png" || $ext_2 == 'jpeg' || $ext_2 == "jpg" || $ext_2 == "png") {

                        if (!empty($file_banner) && $file_banner != null) {
                            $file_name_flag = time() . "." . $ext;
                            $path_of_file = "uploads/banners/escuelas/";
                            $file_banner->move($path_of_file, $file_name_flag);

                            //Update table image
                            $image_flag = $em->getRepository('AdminBundle:Image')->find($catEscuela->getBanner()->getId());
                            $image_flag->setName($file_name_flag);
                            $image_flag->setPath("uploads/banners/escuelas/" . $file_name_flag);
                            $image_flag->setType("banners");

                            $em->persist($image_flag);
                            $em->flush();
                        }

                        if (!empty($file_modal) && $file_modal != null) {
                            $file_name_modal = time() . "." . $ext_2;
                            $path_of_file_2 = "uploads/logos/escuelas/";
                            $file_modal->move($path_of_file_2, $file_name_modal);

                            //Update table image
                            $image_modal = $em->getRepository('AdminBundle:Image')->find($catEscuela->getEsc19()->getId());
                            $image_modal->setName($file_name_modal);
                            $image_modal->setPath("uploads/logos/escuelas/" . $file_name_modal);
                            $image_modal->setType("logos");

                            $em->persist($image_modal);
                            $em->flush();
                        }

                        $catEscuela->setIdPais($em->getRepository('AdminBundle:CatPaisesEscuelas')->find($pais));
                        $catEscuela->setIdCiudad($em->getRepository('AdminBundle:CatCiudadesEscuelas')->findOneBy(array('idCiudad' => $ciudad)));

                        $em->persist($catEscuela);
                        $em->flush();


                        $msg = "Se edito correctamente la escuela";
                        $this->session->getFlashBag()->add("success", $msg);
                        return $this->redirectToRoute('catescuelas_index');


                    } else {
                        $msg = "Formato invalido para el archivo";
                        $this->session->getFlashBag()->add("danger", $msg);
                    }

                }


            } else {
                $this->getDoctrine()->getManager()->flush();

                $msg = "Se edito correctamente la escuela";
                $this->session->getFlashBag()->add("success", $msg);

                return $this->redirectToRoute('catescuelas_index');
            }
        }

        return $this->render('AdminBundle:CatEscuelas:edit.html.twig', array(
            'catEscuela' => $catEscuela,
            'edit_form' => $editForm->createView(),
            'content' => $content,
            'paises' => $paises
        ));
    }

    /**
     * Deletes a catEscuela entity.
     *
     */
    public function deleteAction(Request $request, $idEscuela = null)
    {

        $em = $this->getDoctrine()->getManager();
        $escuela = $em->getRepository('AdminBundle:CatEscuelas')->find($idEscuela);

        //Remove dependencias

        $photogallery = $em->getRepository('AdminBundle:Photogallery')->findOneBy(array(
            'escuela' => $idEscuela
        ));

        if (count($photogallery) > 0) {
            $status = '¡Cuidado! Para borrar la escuela, elimina su multimedia';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('catescuelas_index');
        } else {

            //Remove Image
            if ($escuela->getBanner() != null) {
                $image = $em->getRepository('AdminBundle:Image')->find($escuela->getBanner()->getId());
                $em->remove($image);
                $em->flush();
            }

            if ($escuela->getEsc19() != null) {
                $image = $em->getRepository('AdminBundle:Image')->find($escuela->getEsc19()->getId());
                $em->remove($image);
                $em->flush();
            }

            $em->remove($escuela);
            $flush = $em->flush();

            if ($flush == null) {
                $status = 'La escuela se ha borrado correctamente';
                $this->session->getFlashBag()->add("success", $status);

                return $this->redirectToRoute('catescuelas_index');
            } else {
                $status = 'La escuela no se ha borrado correctamente';
                $this->session->getFlashBag()->add("danger", $status);

                return $this->redirectToRoute('catescuelas_index');
            }
        }

    }

    /**
     * Creates a form to delete a catEscuela entity.
     *
     * @param CatEscuelas $catEscuela The catEscuela entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createDeleteForm(CatEscuelas $catEscuela)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catescuelas_delete', array('idEscuela' => $catEscuela->getIdescuela())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public
    function searchAction(Request $request)
    {
        $search = $request->request->get('search');
        $em = $this->getDoctrine()->getManager();

        $escuelas = $em->getRepository('AdminBundle:CatEscuelas')->findByLettersx($search);

        $data_array = array();
        foreach ($escuelas as $value) {
            $data_array[] = array(
                'id' => $value->getIdEscuela(),
                'name' => $value->getEscNombre(),
                'ciudad' => $value->getIdCiudad()->getCiudadDesc(),
                'pais' => $value->getIdPais()->getPaisDesc()
            );
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($data_array));

        return $response;

    }
}
