<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Idioma;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Idioma controller.
 *
 */
class IdiomaController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all idioma entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $idiomas = $em->getRepository('AdminBundle:Idioma')->findAll();

        return $this->render('AdminBundle:Idioma:index.html.twig', array(
            'idiomas' => $idiomas,
        ));
    }

    /**
     * Creates a new idioma entity.
     *
     */
    public function newAction(Request $request)
    {
        $idioma = new Idioma();
        $form = $this->createForm('AdminBundle\Form\IdiomaType', $idioma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($idioma);
            $em->flush();

            return $this->redirectToRoute('idiomas_index');
        }

        return $this->render('AdminBundle:Idioma:new.html.twig', array(
            'idioma' => $idioma,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a idioma entity.
     *
     */
    public function showAction(Idioma $idioma)
    {
        $deleteForm = $this->createDeleteForm($idioma);

        return $this->render('AdminBundle:Idioma:show.html.twig', array(
            'idioma' => $idioma,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing idioma entity.
     *
     */
    public function editAction(Request $request, Idioma $idioma)
    {
        $deleteForm = $this->createDeleteForm($idioma);
        $editForm = $this->createForm('AdminBundle\Form\IdiomaType', $idioma);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('idiomas_index');
        }

        return $this->render('AdminBundle:Idioma:edit.html.twig', array(
            'idioma' => $idioma,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a idioma entity.
     *
     */
    public function deleteAction(Request $request, $id = null) {

        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:Idioma')->find($id);

        $idioma_content = $em->getRepository('AdminBundle:IdiomaContent')->findBy(array(
            'idioma' => $id
        ));

        if($idioma_content != null){
            $status = 'Para remover este idioma, primero borra su contenido';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('idiomas_index');
        }

        $em->remove($content);
        $flush = $em->flush();

        if ($flush == null) {
            $status = 'El idioma se ha borrado correctamente';
            $this->session->getFlashBag()->add("success", $status);

            return $this->redirectToRoute('idiomas_index');
        } else {
            $status = 'El idioma no se ha borrado correctamente';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('idiomas_index');
        }
    }

    /**
     * Creates a form to delete a idioma entity.
     *
     * @param Idioma $idioma The idioma entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Idioma $idioma)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('idiomas_delete', array('id' => $idioma->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
