<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\CatCiudadesEscuelas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AdminBundle\Entity\Image;

/**
 * Catciudadesescuela controller.
 *
 */
class CatCiudadesEscuelasController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all catCiudadesEscuela entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $catCiudadesEscuelas = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findAll();

        return $this->render('AdminBundle:CatCiudadesEscuelas:index.html.twig', array(
            'catCiudadesEscuelas' => $catCiudadesEscuelas,
        ));
    }

    /**
     * Creates a new catCiudadesEscuela entity.
     *
     */
    public function newAction(Request $request)
    {
        $catCiudadesEscuela = new CatCiudadesEscuelas();
        $form = $this->createForm('AdminBundle\Form\CatCiudadesEscuelasType', $catCiudadesEscuela);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //file flag
            $file_post = $form->get("imgBandera")->getData();
            //file modal
            $file_modal = $form->get("imgModal")->getData();
            $title = $form->get("ciudadDesc")->getData();

            if (!empty($file_post) && $file_post != null && !empty($file_modal) && $file_modal != null) {

                $ext = $file_post->guessExtension();
                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/banderas/ciudades/";
                    $file_post->move($path_of_file, $file_name);

                    //insert table image
                    $image_flag = new Image();
                    $image_flag->setName($file_name);
                    $image_flag->setPath("uploads/banderas/ciudades/" . $file_name);
                    $image_flag->setCreatedAt(new \DateTime());
                    $image_flag->setType("banderas");
                    $em->persist($image_flag);
                    $em->flush();

                    /*********/

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/popups/ciudades/";
                    $file_modal->move($path_of_file, $file_name);

                    //insert table image
                    $image_modal = new Image();
                    $image_modal->setName($file_name);
                    $image_modal->setPath("uploads/popups/ciudades/" . $file_name);
                    $image_modal->setCreatedAt(new \DateTime());
                    $image_modal->setType("popups");
                    $em->persist($image_modal);
                    $em->flush();

                    //Last ciudad in DB
                    $repository = $em->getRepository('AdminBundle:CatCiudadesEscuelas');
                    $ultimo = $repository->createQueryBuilder('t')
                        ->where('t.idCiudad IS NOT NULL')
                        ->orderBy('t.idCiudad', 'DESC')
                        ->getQuery()->setMaxResults(1)->getOneOrNullResult();

                    if ($ultimo != null) {
                        $data = $ultimo->getIdCiudad();
                    } else {
                        $data = 0;
                    }

                    $catCiudadesEscuela->setIdCiudad($data + 1);

                    //Insert ids image
                    $image_db = $em->getRepository('AdminBundle:Image')->find($image_flag->getId());
                    $catCiudadesEscuela->setImgBandera($image_db);
                    $image_db = $em->getRepository('AdminBundle:Image')->find($image_modal->getId());
                    $catCiudadesEscuela->setImgModal($image_db);
                    $em->persist($catCiudadesEscuela);
                    $em->flush();

                    $msg = "Se creo correctamente la Ciudad";
                    $this->session->getFlashBag()->add("success", $msg);


                    return $this->redirectToRoute('catciudadesescuelas_index');
                } else {
                    $msg = "Formato invalido";
                    $this->session->getFlashBag()->add("danger", $msg);
                }
            } else {
                $catCiudadesEscuela->setImgBandera(null);
                $catCiudadesEscuela->setImgModal(null);
                $em->persist($catCiudadesEscuela);
                $em->flush();
            }
        }

        return $this->render('AdminBundle:CatCiudadesEscuelas:new.html.twig', array(
            'catCiudadesEscuela' => $catCiudadesEscuela,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing catCiudadesEscuela entity.
     *
     */
    public function editAction(Request $request, CatCiudadesEscuelas $catCiudadesEscuela)
    {
        $editForm = $this->createForm('AdminBundle\Form\CatCiudadesEscuelasType', $catCiudadesEscuela);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $id = $catCiudadesEscuela->getIdCiudad();
        $content = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->find($id);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file_post = $editForm->get("imgBandera")->getData();
            $file_modal = $editForm->get("imgModal")->getData();

            if ((!empty($file_post) && $file_post != null) || (!empty($file_modal) && $file_modal != null)) {

                $ext = '';
                $ext_2 = '';
                if ($file_post != null) {
                    $ext = $file_post->guessExtension();
                }
                if ($file_modal != null) {
                    $ext_2 = $file_modal->guessExtension();
                }

                if ($catCiudadesEscuela->getImgBandera() == null || $catCiudadesEscuela->getImgModal() == null) {
                    //If image not exist
                    if ($catCiudadesEscuela->getImgBandera() == null && $file_post != null) {

                        $file_name_flag = time() . "." . $ext;
                        $path_of_file = "uploads/banderas/ciudades/";
                        $file_post->move($path_of_file, $file_name_flag);

                        //insert table image
                        $image_flag = new Image();
                        $image_flag->setName($file_name_flag);
                        $image_flag->setPath("uploads/banderas/ciudades/" . $file_name_flag);
                        $image_flag->setCreatedAt(new \DateTime());
                        $image_flag->setType("banderas");
                        $em->persist($image_flag);
                        $em->flush();

                        $catCiudadesEscuela->setImgBandera($image_flag);
                        $em->persist($catCiudadesEscuela);
                        $em->flush();

                    }
                    if ($catCiudadesEscuela->getImgModal() == null && $file_modal != null) {

                        $file_name = time() . "." . $ext_2;
                        $path_of_file = "uploads/popups/ciudades/";
                        $file_modal->move($path_of_file, $file_name);

                        //insert table image
                        $image_modal = new Image();
                        $image_modal->setName($file_name);
                        $image_modal->setPath("uploads/popups/ciudades/" . $file_name);
                        $image_modal->setCreatedAt(new \DateTime());
                        $image_modal->setType("popups");
                        $em->persist($image_modal);
                        $em->flush();

                        $catCiudadesEscuela->setImgModal($image_modal);
                        $em->persist($catCiudadesEscuela);
                        $em->flush();
                    }
                    $msg = "Se edito correctamente la ciudad";
                    $this->session->getFlashBag()->add("success", $msg);


                    return $this->redirectToRoute('catciudadesescuelas_index');

                } else {

                    if ($ext == 'jpeg' || $ext == "jpg" || $ext == "png" || $ext_2 == 'jpeg' || $ext_2 == "jpg" || $ext_2 == "png") {

                        if (!empty($file_post) && $file_post != null) {
                            $file_name_flag = time() . "." . $ext;
                            $path_of_file = "uploads/banderas/ciudades/";
                            $file_post->move($path_of_file, $file_name_flag);

                            //Update table image
                            $image_flag = $em->getRepository('AdminBundle:Image')->find($catCiudadesEscuela->getImgBandera()->getId());
                            $image_flag->setName($file_name_flag);
                            $image_flag->setPath("uploads/banderas/ciudades/" . $file_name_flag);
                            $image_flag->setType("banderas");

                            $em->persist($image_flag);
                            $em->flush();
                        }

                        if (!empty($file_modal) && $file_modal != null) {
                            $file_name_modal = time() . "." . $ext_2;
                            $path_of_file_2 = "uploads/popups/ciudades/";
                            $file_modal->move($path_of_file_2, $file_name_modal);

                            //Update table image
                            $image_modal = $em->getRepository('AdminBundle:Image')->find($catCiudadesEscuela->getImgModal()->getId());
                            $image_modal->setName($file_name_modal);
                            $image_modal->setPath("uploads/popups/ciudades/" . $file_name_modal);
                            $image_modal->setType("popups");

                            $em->persist($image_modal);
                            $em->flush();
                        }

                        $msg = "Se edito correctamente la Ciudad";
                        $this->session->getFlashBag()->add("success", $msg);

                        return $this->redirectToRoute('catciudadesescuelas_index');
                    } else {
                        $msg = "Formato invalido para el archivo";
                        $this->session->getFlashBag()->add("danger", $msg);
                    }
                }


            } else {
                $this->getDoctrine()->getManager()->flush();

                $msg = "Se edito correctamente la ciudad";
                $this->session->getFlashBag()->add("success", $msg);

                return $this->redirectToRoute('catciudadesescuelas_index');
            }
        }

        return $this->render('AdminBundle:CatCiudadesEscuelas:edit.html.twig', array(
            'catCiudadesEscuela' => $catCiudadesEscuela,
            'edit_form' => $editForm->createView(),
            'content' => $content
        ));
    }

    /**
     * Deletes a catCiudadesEscuela entity.
     *
     */
    public function deleteAction(Request $request, $idCiudad = null)
    {
        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->find($idCiudad);

        $escuelas = $em->getRepository('AdminBundle:CatEscuelas')->findBy(array(
            'idCiudad' => $idCiudad
        ));

        if (count($escuelas) > 0) {
            $status = '¡Cuidado!, esta ciudad tiene escuela(s) asignadas';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('catciudadesescuelas_index');
        } else {
            //Remove Image
            if ($content->getImgBandera() != null) {
                $image = $em->getRepository('AdminBundle:Image')->find($content->getImgBandera()->getId());
                $em->remove($image);
                $em->flush();
            }

            if ($content->getImgModal() != null) {
                $image = $em->getRepository('AdminBundle:Image')->find($content->getImgModal()->getId());
                $em->remove($image);
                $em->flush();
            }

            $em->remove($content);
            $flush = $em->flush();

            if ($flush == null) {
                $status = 'La ciudad se ha borrado correctamente';
                $this->session->getFlashBag()->add("success", $status);

                return $this->redirectToRoute('catciudadesescuelas_index');
            } else {
                $status = 'La ciudad no se ha borrado correctamente';
                $this->session->getFlashBag()->add("danger", $status);

                return $this->redirectToRoute('catciudadesescuelas_index');
            }
        }

    }

    /**
     * Creates a form to delete a catCiudadesEscuela entity.
     *
     * @param CatCiudadesEscuelas $catCiudadesEscuela The catCiudadesEscuela entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CatCiudadesEscuelas $catCiudadesEscuela)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('catciudadesescuelas_delete', array('idCiudad' => $catCiudadesEscuela->getIdciudad())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
