<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\IdiomaContent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AdminBundle\Entity\Image;

/**
 * Idiomacontent controller.
 *
 */
class IdiomaContentController extends Controller
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all idiomaContent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $idiomaContents = $em->getRepository('AdminBundle:IdiomaContent')->findAll();

        return $this->render('AdminBundle:Idioma\IdiomaContent:index.html.twig', array(
            'idiomaContents' => $idiomaContents,
        ));
    }

    /**
     * Creates a new idiomaContent entity.
     *
     */
    public function newAction(Request $request)
    {
        $idiomaContent = new Idiomacontent();
        $form = $this->createForm('AdminBundle\Form\IdiomaContentType', $idiomaContent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $file_post = $form->get("file")->getData();

            if (!empty($file_post) && $file_post != null) {

                $ext = $file_post->guessExtension();
                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/popups/idiomas/";
                    $file_post->move($path_of_file, $file_name);

                    //Insert table image
                    $image = new Image();
                    $image->setName($file_name);
                    $image->setPath("uploads/popups/idiomas/" . $file_name);
                    $image->setCreatedAt( new \DateTime() );
                    $image->setType("popups");

                    $em->persist($image);
                    $em->flush();

                    //Inserto Id image
                    $image_db = $em->getRepository('AdminBundle:Image')->find($image->getId());
                    $idiomaContent->setImage($image_db);
                    $em->persist($idiomaContent);
                    $flush = $em->flush();

                    if ($flush == null) {
                        $msg = "Se creo correctamente el Contenido del Idioma";
                        $this->session->getFlashBag()->add("success", $msg);
                    }

                    return $this->redirectToRoute('idiomacontent_index');
                } else {
                    $msg = "Formato invalido";
                    $this->session->getFlashBag()->add("danger", $msg);
                }
            } else {
                $idiomaContent->setImage(null);
                $em->persist($idiomaContent);
                $em->flush();
            }
        }

        return $this->render('AdminBundle:Idioma\IdiomaContent:new.html.twig', array(
            'idiomaContent' => $idiomaContent,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing idiomaContent entity.
     *
     */
    public function editAction(Request $request, IdiomaContent $idiomaContent)
    {
        $editForm = $this->createForm('AdminBundle\Form\IdiomaContentType', $idiomaContent);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $file_post = $editForm->get("file")->getData();
            $title = $editForm->get("title")->getData();

            if (!empty($file_post) && $file_post != null) {

                $ext = $file_post->guessExtension();
                if ($ext === 'jpeg' || $ext == "jpg" || $ext == "png") {

                    $file_name = time() . "." . $ext;
                    $path_of_file = "uploads/popups/idiomas";
                    $file_post->move($path_of_file, $file_name);

                    //Update table image
                    $image = $em->getRepository('AdminBundle:Image')->find($idiomaContent->getImage()->getId());
                    $image->setName($file_name);
                    $image->setPath("uploads/popups/idiomas/" . $file_name);
                    $image->setType("popups");

                    $em->persist($image);
                    $flush = $em->flush();

                    if ($flush == null) {
                        $msg = "Se edito correctamente el Contenido del Idioma!!!";
                        $this->session->getFlashBag()->add("success", $msg);
                    }

                    return $this->redirectToRoute('idiomacontent_index');
                } else {
                    $msg = "Formato invalido para el archivo";
                    $this->session->getFlashBag()->add("danger", $msg);
                }
            } else {
                $this->getDoctrine()->getManager()->flush();

                $msg = "Se edito correctamente el Contenido del Idioma";
                $this->session->getFlashBag()->add("success", $msg);

                return $this->redirectToRoute('idiomacontent_index');
            }

        }

        $id = $idiomaContent->getId();
        $content = $em->getRepository('AdminBundle:IdiomaContent')->find($id);

        return $this->render('AdminBundle:Idioma\IdiomaContent:edit.html.twig', array(
            'idiomaContent' => $idiomaContent,
            'edit_form' => $editForm->createView(),
            'content' => $content
        ));
    }

    /**
     * Deletes a idiomaContent entity.
     *
     */
    public function deleteAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:IdiomaContent')->find($id);

        $em->remove($content);
        $flush = $em->flush();

        if ($flush == null) {
            $status = 'El contenido del idioma se ha borrado correctamente';
            $this->session->getFlashBag()->add("success", $status);

            return $this->redirectToRoute('idiomacontent_index');
        } else {
            $status = 'El contenido del idioma no se ha borrado correctamente';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('idiomacontent_index');
        }

    }

    /**
     * Creates a form to delete a idiomaContent entity.
     *
     * @param IdiomaContent $idiomaContent The idiomaContent entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(IdiomaContent $idiomaContent)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('idiomacontent_delete', array('id' => $idiomaContent->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
