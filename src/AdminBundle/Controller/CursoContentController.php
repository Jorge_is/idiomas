<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\CursoContent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;

/**
 * Cursocontent controller.
 *
 */
class CursoContentController extends Controller
{

    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all cursoContent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cursoContents = $em->getRepository('AdminBundle:CursoContent')->findAll();

        return $this->render('AdminBundle:CatCurso\CursoContent:index.html.twig', array(
            'cursoContents' => $cursoContents,
        ));
    }

    /**
     * Creates a new cursoContent entity.
     *
     */
    public function newAction(Request $request)
    {
        $cursoContent = new CursoContent();
        $form = $this->createForm('AdminBundle\Form\CursoContentType', $cursoContent);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $paises = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            //Name curso
            $curso = $request->get('curso');

            $cursoContent->setCurso($em->getRepository('AdminBundle:CatCursos')->findOneBy(array('curNombre' => $curso)));

            $curso_db =  $em->getRepository('AdminBundle:CatCursos')->findOneBy(array('curNombre' => $curso));
            $res_esc_cur = $em->getRepository('AdminBundle:RegEscuelasCursos')->findOneBy(array('idCurso' => $curso_db->getIdCurso()));

            $cursoContent->setEscuela($em->getRepository('AdminBundle:CatEscuelas')->findOneBy(array('idEscuela' => $res_esc_cur->getIdEscuela())));

            $em->persist($cursoContent);
            $flush = $em->flush();

            if ($flush == null) {
                $msg = "Se creo correctamente el contenido del curso";
                $this->session->getFlashBag()->add("success", $msg);
            }

            return $this->redirectToRoute('cursocontent_index');
        }

        return $this->render('AdminBundle:CatCurso\CursoContent:new.html.twig', array(
            'cursoContent' => $cursoContent,
            'form' => $form->createView(),
            'paises' => $paises
        ));
    }

    /**
     * Finds and displays a cursoContent entity.
     *
     */
    public function showAction(CursoContent $cursoContent)
    {
        $deleteForm = $this->createDeleteForm($cursoContent);

        return $this->render('cursocontent/show.html.twig', array(
            'cursoContent' => $cursoContent,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cursoContent entity.
     *
     */
    public function editAction(Request $request, CursoContent $cursoContent)
    {
        $editForm = $this->createForm('AdminBundle\Form\CursoContentType', $cursoContent);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $id = $cursoContent->getId();
        $content = $em->getRepository('AdminBundle:CursoContent')->find($id);
        $paises = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //Curso Id
            $curso = $request->get('curso');

            $cursoContent->setCurso($em->getRepository('AdminBundle:CatCurso')->findOneBy(array('name' => $curso)));

            $em->persist($cursoContent);
            $flush = $em->flush();

            if ($flush == null) {
                $msg = "Se edito correctamente el Contenido del Curso";
                $this->session->getFlashBag()->add("success", $msg);
            }

            return $this->redirectToRoute('cursocontent_index');

        }

        return $this->render('AdminBundle:CatCurso\CursoContent:edit.html.twig', array(
            'cursoContent' => $cursoContent,
            'edit_form' => $editForm->createView(),
            'content' => $content,
            'paises' => $paises
        ));
    }

    /**
     * Deletes a cursoContent entity.
     *
     */
    public function deleteAction(Request $request, $id = null) {

        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository('AdminBundle:CursoContent')->find($id);

        $em->remove($content);
        $flush = $em->flush();

        if ($flush == null) {
            $status = 'El contenido del curso se ha borrado correctamente';
            $this->session->getFlashBag()->add("success", $status);

            return $this->redirectToRoute('cursocontent_index');
        } else {
            $status = 'El contenido del curso no se ha borrado correctamente';
            $this->session->getFlashBag()->add("danger", $status);

            return $this->redirectToRoute('cursocontent_index');
        }
    }

    /**
     * Creates a form to delete a cursoContent entity.
     *
     * @param CursoContent $cursoContent The cursoContent entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CursoContent $cursoContent)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cursocontent_delete', array('id' => $cursoContent->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function searchAction(Request $request)
    {
        $search = $request->request->get('search');
        $em = $this->getDoctrine()->getManager();

        $cursos = $em->getRepository('AdminBundle:CatCursos')->findByLetters($search);

        $data_array = array();
        foreach ($cursos as $value){
            $data_array[] = array(
                'id' => $value->getIdCurso(),
                'name' => $value->getCurNombre()
            );
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($data_array));

        return $response;

    }
}
