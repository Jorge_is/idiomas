<?php

namespace WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('WebBundle:Default:index.html.twig');
    }

    public function idiomas1Action()
    {
        $em = $this->getDoctrine()->getManager();
        $idiomas = $em->getRepository('AdminBundle:Idioma')->findAll();
        $idiomas_content = $em->getRepository('AdminBundle:IdiomaContent')->findAll();

        return $this->render('WebBundle:Idiomas:idiomas1.html.twig', array(
            'idiomas' => $idiomas,
            'idiomas_content' => $idiomas_content
        ));
    }

    public function idiomas2Action()
    {
        $em = $this->getDoctrine()->getManager();
        $paises = $em->getRepository('AdminBundle:CatPaisesEscuelas')->findAll();
        $ciudades = $em->getRepository('AdminBundle:CatCiudadesEscuelas')->findAll();

        return $this->render('WebBundle:Idiomas:idiomas2.html.twig', array(
            'paises' => $paises,
            'ciudades' => $ciudades
        ));
    }

    public function idiomas3Action()
    {
        $em = $this->getDoctrine()->getManager();
        $objetivos = $em->getRepository('AdminBundle:Objetivo')->findAll();
        $subobjetivos = $em->getRepository('AdminBundle:SubObjetivo')->findAll();


        return $this->render('WebBundle:Idiomas:idiomas3.html.twig', array(
            'objetivos' => $objetivos,
            'subobjetivos' => $subobjetivos,
        ));
    }

    public function idiomas4Action()
    {
        $em = $this->getDoctrine()->getManager();
        $alojamientos = $em->getRepository('AdminBundle:Alojamiento')->findAll();

        return $this->render('WebBundle:Idiomas:idiomas4.html.twig', array(
            'alojamientos' => $alojamientos
        ));
    }

    public function idiomas5Action()
    {
        $em = $this->getDoctrine()->getManager();

        $escuelas = $em->getRepository('AdminBundle:CatEscuelas')->findBy(array(
            'idPais' => 1,
            'idCiudad' => 1
        ));

        $cursos_content = $em->getRepository('AdminBundle:CursoContent')->findAll();
        $blocks = $em->getRepository('AdminBundle:Block')->findAll();
        $secciones = $em->getRepository('AdminBundle:SeccionesEscuelas')->findAll();
        $photogalleries = $em->getRepository('AdminBundle:Photogallery')->findAll();

        return $this->render('WebBundle:Idiomas:idiomas5.html.twig', array(
            'escuelas' => $escuelas,
            'cursos_content' => $cursos_content,
            'blocks' => $blocks,
            'secciones' => $secciones,
            'photogalleries' => $photogalleries
        ));
    }

    public function busquedaGeneralAction()
    {
        return $this->render('WebBundle:Idiomas:busqueda-general.html.twig');
    }
}
